{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Triplet excitations\n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/13/tutorial/response/triplet_excitations/)\n",
    "\n",
    "In this tutorial, we will calculate triplet excitations for methane with time-propagation and Casida methods.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "import subprocess"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir 5_Triplet_excitations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 5_Triplet_excitations/"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Time-propagation\n",
    "\n",
    "### Ground-state\n",
    "We begin with a spin-polarized calculation of the ground-state, as before but with <code><a href=https://www.octopus-code.org/documentation//13/variables/states/spincomponents>SpinComponents</a> = spin_polarized</code> specified now.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs_triplet.txt'\n",
    "stderr = 'stderr_gs_triplet.txt'\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "SpinComponents = spin_polarized"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "You can verify that the results are identical in detail to the non-spin-polarized calculation since this is a non-magnetic system.\n",
    "\n",
    "### Time-propagation\n",
    "\n",
    "Next, we perform the time-propagation using the following input file:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_td_triplet.txt'\n",
    "stderr = 'stderr_td_triplet.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "SpinComponents = spin_polarized\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDTimeStep = 0.004/eV\n",
    "TDMaxSteps = 2500  # ~ 10.0/TDTimeStep\n",
    "\n",
    "TDDeltaStrength = 0.01/angstrom\n",
    "TDPolarizationDirection = 1\n",
    "TDDeltaStrengthMode = kick_spin"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "Besides the [SpinComponents](https://www.octopus-code.org/documentation//13/variables/states/spincomponents) variable, the main difference is the type of perturbation that is applied to the system. By setting <code><a href=https://www.octopus-code.org/documentation//13/variables/time-dependent/response/tddeltastrengthmode>TDDeltaStrengthMode</a> = kick_spin</code>, the kick will have opposite sign for up and down states. Whereas the ordinary kick (`kick_density`) yields the response to a homogeneous electric field, *i.e.* the electric dipole response, this kick yields the response to a homogeneous magnetic field, *i.e.* the magnetic dipole response. Note however that only the spin degree of freedom is coupling to the field; a different calculation would be required to obtain the orbital part of the response. Only singlet excited states contribute to the spectrum with `kick_density`, and only triplet excited states contribute with `kick_spin`. We will see below how to use symmetry to obtain both at once with `kick_spin_and_density`.\n",
    "\n",
    "\n",
    "The time requiered to create all runs are in the order of 40 minutes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "### Spectrum\n",
    "\n",
    "When the propagation completes, we run the [oct-propagation_spectrum](https://www.octopus-code.org/documentation/13/manual/external_utilities/oct-propagation_spectrum) utility to obtain the spectrum.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-propagation_spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mv cross_section_vector cross_section_vector_triplet"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "Recalculate the singlet for comparisment"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs_singlet.txt'\n",
    "stderr = 'stderr_gs_singlet.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "FromScratch = yes\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_td_singlet.txt'\n",
    "stderr = 'stderr_td_singlet.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "FromScratch = yes\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDTimeStep = 0.0023/eV\n",
    "TDMaxSteps = 4350  # ~ 10.0/TDTimeStep\n",
    "\n",
    "TDDeltaStrength = 0.01/angstrom\n",
    "TDPolarizationDirection = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "The time requiered to create all runs are in the order of 5 minutes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-propagation_spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "with open(\"cross_section_vector_triplet\") as csv_triplet:\n",
    "    print(\"\".join(csv_triplet.readlines()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mv cross_section_vector cross_section_vector_singlet"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "with open(\"cross_section_vector_singlet\") as csv_singlet:\n",
    "    print(\"\".join(csv_singlet.readlines()))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "## Comparison of absorption spectrum of methane calculated with time-propagation for singlets and triplets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.set_xlabel(\"Energy (eV)\")\n",
    "ax.set_ylabel(\"Strength function (1/eV)\")\n",
    "\n",
    "\n",
    "df_singlet = pd.read_csv(\n",
    "    \"cross_section_vector_singlet\",\n",
    "    delimiter=\"     \",\n",
    "    usecols=[0, 4],\n",
    "    names=[\"Energy (eV)\", \"StrengthFunction\"],\n",
    "    skiprows=26,\n",
    "    engine=\"python\",\n",
    ")\n",
    "\n",
    "\n",
    "df_triplet = pd.read_csv(\n",
    "    \"cross_section_vector_triplet\",\n",
    "    delimiter=\"     \",\n",
    "    usecols=[0, 4],\n",
    "    names=[\"Energy (eV)\", \"StrengthFunction\"],\n",
    "    skiprows=26,\n",
    "    engine=\"python\",\n",
    ")\n",
    "\n",
    "\n",
    "df_triplet[\"StrengthFunction\"] = -1 * df_triplet[\"StrengthFunction\"]\n",
    "\n",
    "\n",
    "df_singlet.plot(\n",
    "    x=\"Energy (eV)\",\n",
    "    y=\"StrengthFunction\",\n",
    "    ax=ax,\n",
    "    label=\"singlet\",\n",
    "    xlim=[7, 15],\n",
    "    color=\"purple\",\n",
    ")\n",
    "\n",
    "\n",
    "df_triplet.plot(\n",
    "    x=\"Energy (eV)\",\n",
    "    y=\"StrengthFunction\",\n",
    "    ax=ax,\n",
    "    label=\"triplet\",\n",
    "    xlim=[7, 20],\n",
    "    color=\"green\",\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26",
   "metadata": {},
   "source": [
    "You can see that there are now separate columns for cross-section and strength function for each spin. The physically meaningful strength function for the magnetic excitation is given by `StrengthFunction(1)` - `StrengthFunction(2)` (since the kick was opposite for the two spins). (If we had obtained `cross_section_tensor`, then the trace in the second column would be the appropriate cross-section to consider.) We can plot and compare to the singlet results obtained before. You can see how this looks on the right. The first triplet transition is found at 9.05 eV, slightly lower energy than the lowest singlet transition.\n",
    "\n",
    "If you are interested, you can also repeat the calculation for <code><a href=https://www.octopus-code.org/documentation//13/variables/time-dependent/response/tddeltastrengthmode>TDDeltaStengthMode</a> =TDDeltaStrengthMode </code> (the default) and confirm that the result is the same as for the non-spin-polarized calculation.\n",
    "\n",
    "### Using symmetries of non-magnetic systems\n",
    "\n",
    "As said before, methane is a non-magnetic system, that is, the up and down densities are the same and the magnetization density is zero everywhere:\n",
    "\n",
    "$$\n",
    " \\rho^{\\uparrow}(\\mathbf r) = \\rho^{\\downarrow}(\\mathbf r)\n",
    "$$\n",
    "\n",
    "Note that it is not enough that the total magnetic moment of the system is zero as the previous condition does not hold for anti-ferromagnetic systems. The symmetry in the spin-densities can actually be exploited in order to obtain both the singlet and triplet spectra with a single calculation. This is done by using a special perturbation that is only applied to the spin up states.[$^1$](#first_reference)\n",
    "To use this perturbation, we need to set <code><a href=https://www.octopus-code.org/documentation//13/variables/time-dependent/response/tddeltastrengthmode>TDDeltaStrengthMode</a> = kick_spin_and_density</code>. If you repeat the time-propagation with this kick, you should obtain a different `cross_section_vector` file containing both the singlet and triplet spectra. The singlets are given by `StrengthFunction(1)` + `StrengthFunction(2)`, while the triplets are given by `StrengthFunction(1)``StrengthFunction(2)`.\n",
    "\n",
    "\n",
    "## Casida equation\n",
    "\n",
    "The calculation of triplets with the `casida` mode for spin-polarized systems is currently not implement in **Octopus**. Nevertheless, just like for the time-propagation, we can take advantage that for non-magnetic systems the two spin are equivalent. In this case it allow us to calculate triplets without the need for a spin-polarized run. The effective kernels in these cases are:\n",
    "\n",
    "$f_{\\rm Hxc}^{\\rm singlet} \\left[ \\rho \\right] = f^{\\uparrow}_{\\rm Hxc} \\left[ \\rho ^{\\uparrow} \\right] + f^{\\uparrow}_{\\rm Hxc} \\left[ \\rho^{\\downarrow} \\right] = f_{\\rm H} \\left[ \\rho \\right] + f^{\\uparrow}_{\\rm xc} \\left[ \\rho ^{\\uparrow} \\right] + f^{\\uparrow}_{\\rm xc} \\left[ \\rho^{\\downarrow} \\right]$\n",
    "\n",
    "$f_{\\rm Hxc}^{\\rm triplet} \\left[ \\rho \\right] = f^{\\uparrow}_{\\rm Hxc} \\left[ \\rho ^{\\uparrow} \\right] - f^{\\uparrow}_{\\rm Hxc} \\left[ \\rho^{\\downarrow} \\right] = f^{\\uparrow}_{\\rm xc} \\left[ \\rho ^{\\uparrow} \\right] - f^{\\uparrow}_{\\rm xc} \\left[ \\rho^{\\downarrow} \\right]$\n",
    "\n",
    "Therefore, we start by doing a ground-state and unoccupied states runs exactly as was done in the [Optical spectra from Casida tutorial](3_Optical_spectra_from_casida.ipynb). Then, do a Casida run with the following input file:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs_casida.txt'\n",
    "stderr = 'stderr_gs_casida.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29",
   "metadata": {},
   "source": [
    "## Unoccupied states"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_unocc_casida.txt'\n",
    "stderr = 'stderr_unocc_casida.txt'\n",
    "\n",
    "CalculationMode = unocc\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "ExtraStates = 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32",
   "metadata": {},
   "source": [
    "## Triplet: Casida"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_triplet_casida.txt'\n",
    "stderr = 'stderr_triplet_casida.txt'\n",
    "\n",
    "CalculationMode = casida\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "ExtraStates = 10\n",
    "\n",
    "CasidaCalcTriplet = yes\n",
    "\n",
    "ExperimentalFeatures = yes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-casida_spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36",
   "metadata": {},
   "outputs": [],
   "source": [
    "subprocess.run(\n",
    "    [\"mv spectrum.casida spectrum.casida_triplet && mv spectrum.casida_triplet ..\"],\n",
    "    shell=True,\n",
    "    cwd=\"casida\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37",
   "metadata": {},
   "source": [
    "## Singlet: Casida"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_singlet_casida.txt'\n",
    "stderr = 'stderr_singlet_casida.txt'\n",
    "\n",
    "CalculationMode = casida\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "ExtraStates = 10\n",
    "\n",
    "ExperimentalFeatures = yes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-casida_spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41",
   "metadata": {},
   "outputs": [],
   "source": [
    "subprocess.run(\n",
    "    [\"mv spectrum.casida spectrum.casida_singlet && mv spectrum.casida_singlet ..\"],\n",
    "    shell=True,\n",
    "    cwd=\"casida\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42",
   "metadata": {},
   "source": [
    "### Comparison of absorption spectrum of methane calculated with the Casida equation for singlets and triplets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.set_xlabel(\"Energy (eV)\")\n",
    "ax.set_ylabel(\"Strength function (1/eV)\")\n",
    "\n",
    "\n",
    "df_casida_singlet = pd.read_csv(\n",
    "    \"spectrum.casida_singlet\",\n",
    "    delimiter=\"  \",\n",
    "    usecols=[0, 4],\n",
    "    names=[\"Energy (eV)\", \"StrengthFunction\"],\n",
    "    skiprows=1,\n",
    "    engine=\"python\",\n",
    ")\n",
    "\n",
    "\n",
    "df_casida_triplet = pd.read_csv(\n",
    "    \"spectrum.casida_triplet\",\n",
    "    delimiter=\"  \",\n",
    "    usecols=[0, 4],\n",
    "    names=[\"Energy (eV)\", \"StrengthFunction\"],\n",
    "    skiprows=1,\n",
    "    engine=\"python\",\n",
    ")\n",
    "\n",
    "\n",
    "df_triplet[\"StrengthFunction\"] = -1 * df_triplet[\"StrengthFunction\"]\n",
    "\n",
    "\n",
    "df_casida_singlet.plot(\n",
    "    x=\"Energy (eV)\",\n",
    "    y=\"StrengthFunction\",\n",
    "    ax=ax,\n",
    "    label=\"singlet\",\n",
    "    xlim=[7, 15],\n",
    "    color=\"purple\",\n",
    ")\n",
    "\n",
    "\n",
    "df_casida_triplet.plot(\n",
    "    x=\"Energy (eV)\",\n",
    "    y=\"StrengthFunction\",\n",
    "    ax=ax,\n",
    "    label=\"triplet\",\n",
    "    xlim=[0, 20],\n",
    "    color=\"green\",\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44",
   "metadata": {},
   "source": [
    "# Comparison"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45",
   "metadata": {},
   "source": [
    "### Comparison of triplet absorption spectrum of methane calculated with time-propagation and with the Casida equation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.set_xlabel(\"Energy (eV)\")\n",
    "ax.set_ylabel(\"Strength function (1/eV)\")\n",
    "\n",
    "\n",
    "df_triplet[\"StrengthFunction\"] = -1 * df_triplet[\"StrengthFunction\"]\n",
    "\n",
    "\n",
    "df_triplet.plot(\n",
    "    x=\"Energy (eV)\",\n",
    "    y=\"StrengthFunction\",\n",
    "    ax=ax,\n",
    "    label=\"time-propagation\",\n",
    "    xlim=[0, 20],\n",
    "    color=\"purple\",\n",
    ")\n",
    "\n",
    "\n",
    "df_casida_triplet.plot(\n",
    "    x=\"Energy (eV)\",\n",
    "    y=\"StrengthFunction\",\n",
    "    ax=ax,\n",
    "    label=\"Casida\",\n",
    "    xlim=[0, 20],\n",
    "    color=\"green\",\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47",
   "metadata": {},
   "source": [
    "[Go to *6_Use_of_symmetries_in_optical_spectra_from_time_propagation.ipynb*](6_Use_of_symmetries_in_optical_spectra_from_time_propagation.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48",
   "metadata": {},
   "source": [
    "## References"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "49",
   "metadata": {},
   "source": [
    "1. M.J.T. Oliveira, A. Castro, M.A.L. Marques, and A. Rubio, On the use of Neumann's principle for the calculation of the polarizability tensor of nanostructures, [J. Nanoscience and Nanotechnology]() 8 1-7 (2008);\n",
    "<span id=\"first_reference\"></span>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
