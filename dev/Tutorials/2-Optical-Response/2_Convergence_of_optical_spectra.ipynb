{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Convergence of the optical spectra\n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/13/tutorial/response/convergence_of_the_optical_spectra/)\n",
    "\n",
    "In the previous [Optical Spectra from TD](1_Optical_spectra_from_time_propagation.ipynb) we have seen how to obtain the absorption spectrum of a molecule. Here we will perform a convergence study of the spectrum with respect to the grid parameters using again the methane molecule as an example.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import os\n",
    "import pandas as pd\n",
    "from string import Template\n",
    "import subprocess"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 2_Convergence_of_optical_spectra"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 2_Convergence_of_optical_spectra"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Convergence with the spacing\n",
    "\n",
    "In the [Total energy convergence tutorial](../1-basics/3-Total_energy_convergence.ipynb) we found out that the total energy of the methane molecule was converged to within 0.1 eV for a spacing of 0.18 Å and a radius of 3.5 Å. We will therefore use these values as a starting point for our convergence study. Like for the total energy, the only way to check if the absorption spectrum is converged with respect to the spacing is by repeating a series of calculations, with identical input files except for the grid spacing. The main difference in this case is that each calculation for a given spacing requires several steps:\n",
    "\n",
    "- Ground-state calculation,\n",
    "- Three time-dependent propagations with perturbations along ''x'', ''y'' and ''z'',\n",
    "- Calculation of the spectrum using the `oct-propagation_spectrum` utility.\n",
    "\n",
    "As we have seen before, the $T_d$ symmetry of methane means that the spectrum is identical for all directions, so in this case we only need to perform the convergence study with a perturbation along the ''x'' direction.\n",
    "\n",
    "All these calculations can be done by hand, but we would rather use a script for this boring an repetitive task. We will then need the following file.\n",
    "\n",
    "#### inp\n",
    "This file is the same as the one used in the previous [Optical Spectra from TD](1_Optical_spectra_from_time_propagation.ipynb):\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "inp_template_methane_spacing = Template(\n",
    "    \"\"\"\n",
    "    stdout = 'stdout_${Calculation_value}_${Spacing_value}.txt'\n",
    "    stderr = 'stderr_${Calculation_value}_${Spacing_value}.txt'\n",
    "\n",
    "    CalculationMode = ${Calculation_value}\n",
    "    FromScratch = yes\n",
    "    UnitsOutput = eV_angstrom\n",
    "\n",
    "    Radius = 3.5*angstrom\n",
    "    Spacing = ${Spacing_value}*angstrom\n",
    "\n",
    "    CH = 1.097*angstrom\n",
    "    %Coordinates\n",
    "    \"C\" |           0 |          0 |           0\n",
    "    \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    "    \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    "    \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    "    \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "    %\n",
    "\n",
    "    TDPropagator = aetrs\n",
    "    TDTimeStep = 0.0023/eV\n",
    "    TDMaxSteps = 4350  # ~ 10.0/TDTimeStep\n",
    "\n",
    "    TDDeltaStrength = 0.01/angstrom\n",
    "    TDPolarizationDirection = 1   \n",
    "    \"\"\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "This script performs the three steps mentioned above for a given list of spacings. You will notice that we are not including smaller spacings than 0.18 Å. This is because we know from previous experience that the optical spectrum is well converged for spacings larger than the one required to converge the total energy.\n",
    "\n",
    "This will take a few minutes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "spacing_list = [0.26, 0.24, 0.22, 0.20, 0.18]\n",
    "\n",
    "for spacing in spacing_list:\n",
    "    print(f\"Running octopus for spacing: {spacing}\")\n",
    "\n",
    "    os.makedirs(f\"methane/variable_spacing/run_{spacing}\")\n",
    "\n",
    "    with open(f\"methane/variable_spacing/run_{spacing}/inp\", \"w\") as inp_file:\n",
    "        inp_file.write(\n",
    "            inp_template_methane_spacing.substitute(\n",
    "                Spacing_value=spacing, Calculation_value=\"gs\"\n",
    "            )\n",
    "        )\n",
    "    subprocess.run(\"octopus\", shell=True, cwd=f\"methane/variable_spacing/run_{spacing}\")\n",
    "\n",
    "    with open(f\"methane/variable_spacing/run_{spacing}/inp\", \"w\") as inp_file:\n",
    "        inp_file.write(\n",
    "            inp_template_methane_spacing.substitute(\n",
    "                Spacing_value=spacing, Calculation_value=\"td\"\n",
    "            )\n",
    "        )\n",
    "    subprocess.run(\n",
    "        \"octopus && oct-propagation_spectrum\",\n",
    "        shell=True,\n",
    "        cwd=f\"methane/variable_spacing/run_{spacing}\",\n",
    "    )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "Once the script is finished running, we need to compare the spectra for the different spacings. You can see on the right how this plot should look like. To make the plot easier to read, we have restricted the energy range and omitted some of the spacings. From the plot it is clear that a spacing of 0.24 Å is sufficient to have the position of the peaks converged to within 0.1 eV."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.set_title(\"Absorption spectrum of methane for different spacings\")\n",
    "ax.set_xlabel(\"Energy (eV)\")\n",
    "ax.set_ylabel(\"Strength function (1/eV)\")\n",
    "\n",
    "for spacing in spacing_list:\n",
    "    df = pd.read_csv(\n",
    "        f\"methane/variable_spacing/run_{spacing}/cross_section_vector\",\n",
    "        delimiter=\"     \",\n",
    "        usecols=[0, 4],\n",
    "        names=[\"Energy (eV)\", \"StrengthFunction\"],\n",
    "        skiprows=26,\n",
    "        engine=\"python\",\n",
    "    )\n",
    "    df.plot(\n",
    "        x=\"Energy (eV)\", y=\"StrengthFunction\", ax=ax, label=f\"{spacing}\", xlim=[9, 15]\n",
    "    )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "## Convergence with the radius\n",
    "\n",
    "We will now study how the spectrum changes with the radius. We will proceed as for the spacing, by performing a series of calculations with identical input files except for the radius. For this we will use the following file.\n",
    "\n",
    "#### inp\n",
    "This file is the same as the prevous one, with the exception of the spacing and the time-step. As we have just seen, a spacing of 0.24 Å is sufficient, which in turns allows us to use a larger time-step.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "inp_template_methane_radius = Template(\n",
    "    \"\"\"\n",
    "    stdout = 'stdout_${Calculation_value}_${Radius_value}.txt'\n",
    "    stderr = 'stderr_${Calculation_value}_${Radius_value}.txt'\n",
    "\n",
    "    CalculationMode = ${Calculation_value}\n",
    "    FromScratch = yes\n",
    "    UnitsOutput = eV_angstrom\n",
    "\n",
    "    Radius = $Radius_value*angstrom\n",
    "    Spacing = 0.24*angstrom\n",
    "\n",
    "    CH = 1.097*angstrom\n",
    "    %Coordinates\n",
    "    \"C\" |           0 |          0 |           0\n",
    "    \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    "    \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    "    \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    "    \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "    %\n",
    "\n",
    "    TDPropagator = aetrs\n",
    "\n",
    "    TDTimeStep = 0.004/eV\n",
    "    TDMaxSteps = 2500  # ~ 10.0/TDTimeStep\n",
    "\n",
    "    TDDeltaStrength = 0.01/angstrom\n",
    "    TDPolarizationDirection = 1\n",
    "    \"\"\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "The time requiered to create all runs are in the order of 11 minutes.\n",
    "This example provides an alternative to the use of subprocess."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "list_of_radii = [3.5, 4.5, 5.5, 6.5, 7.5]\n",
    "\n",
    "for radius in list_of_radii:\n",
    "    print(f\"Running octopus for radius: {radius}\")\n",
    "\n",
    "    os.makedirs(f\"methane/variable_radius/run_{radius}\")\n",
    "\n",
    "    run_path = \"methane/variable_radius/run_\" + str(radius)\n",
    "    inp_file_path = run_path + \"/inp\"\n",
    "\n",
    "    with open(inp_file_path, \"w\") as inp_file:\n",
    "        inp_file.write(\n",
    "            inp_template_methane_radius.substitute(\n",
    "                Radius_value=radius, Calculation_value=\"gs\"\n",
    "            )\n",
    "        )\n",
    "    !cd {run_path} && octopus\n",
    "\n",
    "    with open(inp_file_path, \"w\") as inp_file:\n",
    "        inp_file.write(\n",
    "            inp_template_methane_radius.substitute(\n",
    "                Radius_value=radius, Calculation_value=\"td\"\n",
    "            )\n",
    "        )\n",
    "    !cd {run_path} && octopus && oct-propagation_spectrum"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "Now plot the spectra from the `cross_section_vector` files. We see that in this case the changes are quite dramatic. To get the first peak close to 10 eV converged within 0.1 eV a radius of 6.5 Å is necessary. The converged transition energy of 9.2 eV agrees quite well with the experimental value of 9.6 eV and with the TDDFT value of 9.25 eV obtained by other authors.[$^1$](#first_Reference)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.set_title(\"Absorption spectrum of methane for different radii\")\n",
    "ax.set_xlabel(\"Energy (eV)\")\n",
    "ax.set_ylabel(\"Strength function (1/eV)\")\n",
    "\n",
    "for radius in list_of_radii:\n",
    "    df = pd.read_csv(\n",
    "        f\"methane/variable_radius/run_{radius}/cross_section_vector\",\n",
    "        delimiter=\"     \",\n",
    "        usecols=[0, 4],\n",
    "        names=[\"Energy (eV)\", \"StrengthFunction\"],\n",
    "        skiprows=26,\n",
    "        engine=\"python\",\n",
    "    )\n",
    "    df.plot(\n",
    "        x=\"Energy (eV)\", y=\"StrengthFunction\", ax=ax, label=f\"{radius}\", xlim=[7, 15]\n",
    "    );"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "\n",
    "What about the peaks at higher energies? Since we are running in a box with zero boundary conditions, we will also have the box states in the spectrum. The energy of those states will change with the inverse of the size of the box and the corresponding peaks will keep shifting to lower energies. However, as one is usually only interested in bound-bound transitions in the optical or near-ultraviolet regimes, we will stop our convergence study here. As an exercise you might want to converge the next experimentally observed transition. Just keep in mind that calculations with larger boxes might take quite some time to run.\n",
    "\n",
    "[Go to *3_Optical_spectra_from_casida.ipynb*](3_Optical_spectra_from_casida.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "## References "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "1. N. N. Matsuzawa, A. Ishitani, D. A. Dixon, and T. Uda, Time-Dependent Density Functional Theory Calculations of Photoabsorption Spectra in the Vacuum Ultraviolet Region, [J. Phys. Chem. A](https://doi.org/10.1021/jp003937v) 105 4953–4962 (2001);\n",
    "<span id=\"first_reference\"></span>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
