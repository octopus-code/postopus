{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Optical spectra from sternheimer \n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/13/tutorial/response/optical_spectra_from_sternheimer/)\n",
    "\n",
    "We have just seen how to calculate optical spectra in the time domain with a finite perturbation, and in a frequency-domain, linear-response matrix formulation with the Casida equation. Now we will try a third approach, which is in the frequency domain and linear response but rather than using a pseudo-eigenvalue equation as in Casida, uses a self-consistent linear equation, the Sternheimer equation. This approach is also known as density-functional perturbation theory. It has superior scaling, is more efficient for dense spectra, and is more applicable to nonlinear response. One disadvantage is that one needs to proceed one frequency point at a time, rather than getting the whole spectrum at once. We will find we can obtain equivalent results with this approach for the optical spectra as for time propagation and Casida, by calculating the polarizability and taking the imaginary part.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import math"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 4_Optical_spectra_from_Sternheimer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 4_Optical_spectra_from_Sternheimer"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Ground state\n",
    "\n",
    "Before doing linear response, we need to obtain the ground state of the system, for which we can use the same input file as for [Optical spectra from Casida](3_Optical_spectra_from_casida.ipynb), but we will use a tighter numerical tolerance, which helps the Sternheimer equation to be solved more rapidly. Unlike for Casida, no unoccupied states are required. If they are present, they won't be used anyway.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "ConvRelDens = 1e-7"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "## Linear response\n",
    "\n",
    "Change the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) to `em_resp` and add\n",
    "the lines below to the input file.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_em_resp.txt'\n",
    "stderr = 'stderr_em_resp.txt'\n",
    "\n",
    "CalculationMode = em_resp\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 6.5*angstrom\n",
    "Spacing = 0.24*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "ConvRelDens = 1e-7\n",
    "\n",
    "%EMFreqs\n",
    "5 | 0*eV | 8*eV\n",
    "9 | 10*eV | 12*eV\n",
    "%\n",
    "EMEta = 0.1*eV\n",
    "\n",
    "Preconditioner = no\n",
    "LinearSolver = qmr_dotp\n",
    "ExperimentalFeatures = yes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "The frequencies of interest must be specified, and we choose them based on the what we have seen from the Casida spectrum. The block above specifies 5 frequencies spanning the range 0 to 8 eV (below the resonances) and 9 frequencies spanning the range 10 to 12 eV (where there are peaks). If we didn’t know where to look, then looking at a coarse frequency grid and then sampling more points in the region that seems to have a peak (including looking for signs of resonances in the real part of the polarizability) would be a reasonable approach. We must add a small imaginary part (ηη) to the frequency in order to be able to obtain the imaginary part of the response, and to avoid divergence at resonances. The resonances are broadened into Lorentzians with this width. The larger the ηη, the easier the SCF convergence is, but the lower the resolution of the spectrum.\n",
    "\n",
    "To help in the numerical solution, we turn off the preconditioner (which sometimes causes trouble here), and use a linear solver that is experimental but will give convergence much faster than the default one.\n",
    "\n",
    "The time requiered to run octopus is in the order of half an hour."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "## Spectrum\n",
    "\n",
    "This Python script can extract the needed info out of the files in the `em_resp` directory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "c = 137.035999679\n",
    "\n",
    "# List directories matching 'em_resp/freq*'\n",
    "ls_output = os.listdir(\"em_resp\")\n",
    "freqs = sorted(\n",
    "    [\n",
    "        float(filename.split(\"_\")[1])\n",
    "        for filename in ls_output\n",
    "        if filename.startswith(\"freq_\")\n",
    "    ]\n",
    ")\n",
    "print(\"# Energy (eV)    Re alpha         Im alpha    Cross-section (A^2)\")\n",
    "format_str = \"{:8.3f}     {:13.7f}     {:13.7f}     {:13.7f}\"  # Updated to include 4 decimal places\n",
    "\n",
    "for freq in freqs:\n",
    "    freq_str = f\"{freq:.4f}\"  # Format freq with 4 decimal places\n",
    "    if freq == 0.0000:\n",
    "        Im_alpha = 0\n",
    "        energy = 0\n",
    "        cross_section = 0\n",
    "    else:\n",
    "        cross_section_file = open(f\"em_resp/freq_{freq_str}/cross_section\")\n",
    "        crossbits = cross_section_file.read().split()\n",
    "        energy = float(crossbits[26])\n",
    "        cross_section = float(crossbits[27])  # isotropic average\n",
    "        Im_alpha = c * cross_section / (4 * math.pi * energy)\n",
    "        cross_section_file.close()\n",
    "\n",
    "    alpha_file = open(f\"em_resp/freq_{freq_str}/alpha\")\n",
    "    alphabits = alpha_file.read().split()\n",
    "    Re_alpha = float(alphabits[15])\n",
    "    alpha_file.close()\n",
    "\n",
    "    print(format_str.format(energy, Re_alpha, Im_alpha, cross_section))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "[Go to *5_Triplet_excitations.ipynb*](5_Triplet_excitations.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
