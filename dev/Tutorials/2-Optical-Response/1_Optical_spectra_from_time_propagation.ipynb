{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optical spectra from time-propagation\n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/13/tutorial/response/optical_spectra_from_time-propagation/)\n",
    "\n",
    "In this tutorial we will learn how to obtain the absorption spectrum of a molecule from the explicit solution of the time-dependent Kohn-Sham equations. We choose as a test case methane (CH<sub>4</sub>).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "from postopus import Run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 1_Optical_spectra_from_time_propagation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 1_Optical_spectra_from_time_propagation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ground state\n",
    "\n",
    "Before starting out time-dependent simulations, we need to obtain the ground state of the system. For this we use basically the same inp file as in the\n",
    "[total energy convergence tutorial](../1-basics/3-Total_energy_convergence.ipynb):\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt' \n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 3.5*angstrom\n",
    "Spacing = 0.18*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After running ```octopus```, we will have the Kohn-Sham wave-functions of the ground-state in the directory ```restart/gs```. As we are going to propagate these wave-functions, they have to be well converged. It is important not only to converge the energy (that is relatively easy to converge), but the density.\n",
    "The default [ConvRelDens](https://www.octopus-code.org/documentation//13/variables/scf/convergence/convreldens) = 1e-06 is usually enough, though.\n",
    "\n",
    "## Time-dependent run\n",
    "To calculate absorption, we excite the system with an infinitesimal electric-field pulse, and then propagate the time-dependent Kohn-Sham equations for a certain time ''T''. The spectrum can then be evaluated from the time-dependent dipole moment.\n",
    "\n",
    "#### Input\n",
    "\n",
    "This is how the input file should look for the time propagation. It is similar to the one from the [Time-dependent propagation tutorial](../1-basics/6-TD_propagation.ipynb).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_td.txt'\n",
    "stderr = 'stderr_td.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "FromScratch = yes\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 3.5*angstrom\n",
    "Spacing = 0.18*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDTimeStep = 0.0023/eV\n",
    "TDMaxSteps = 4350  # ~ 10.0/TDTimeStep\n",
    "\n",
    "TDDeltaStrength = 0.01/angstrom\n",
    "TDPolarizationDirection = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Besides changing the [CalculationMode]() to ```td```, we have added [FromScratch]() = ```yes```. This will be useful if you decide to run the propagation for other polarization directions (see bellow). For the time-evolution we use again the Approximately Enforced Time-Reversal Symmetry (aetrs) propagator. The time-step is chosen such that the propagation remains numerically stable. You should have learned how to set it up in the tutorial [time-dependent propagation tutorial](../1-basics/6-TD_propagation.ipynb). Finally, we set the number of time steps with the variable [TDMaxSteps](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdmaxsteps). To have a maximum propagation time of 10 $\\hbar/{\\rm eV}$ we will need around 4350 iterations.\n",
    "\n",
    "We have also introduced two new input variables to define our perturbation:\n",
    "\n",
    "* [TDDeltaStrength](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/tddeltastrength): this is the strength of the perturbation. This number should be small to keep the response linear, but should be sufficiently large to avoid numerical problems.\n",
    "\n",
    "* [TDPolarizationDirection](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationdirection): this variable sets the polarization of our perturbation to be on the first axis (''x'').\n",
    "\n",
    "Note that you will be calculating the singlet dipole spectrum. You can also obtain the triplet by using [TDDeltaStrengthMode](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/tddeltastrengthmode), and other multipole responses by using [TDKickFunction](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/tdkickfunction). For details on triplet calculations see the [Triplet excitations tutorial](../2-Optical-Response/5_Triplet_excitations.ipynb).\n",
    "\n",
    "You can now start ```Octopus``` and go for a quick coffee (this should take a few minutes depending on your machine). Propagations are slow, but the good news is that they scale very well with the size of the system. This means that even if methane is very slow, a molecule with 200 atoms can still be calculated without big problems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mv td.general/multipoles td.general/multipoles.1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_td.txt'\n",
    "stderr = 'stderr_td.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "FromScratch = yes\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 3.5*angstrom\n",
    "Spacing = 0.18*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDTimeStep = 0.0023/eV\n",
    "TDMaxSteps = 4350  # ~ 10.0/TDTimeStep\n",
    "\n",
    "TDDeltaStrength = 0.01/angstrom\n",
    "TDPolarizationDirection = 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mv td.general/multipoles td.general/multipoles.2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_td.txt'\n",
    "stderr = 'stderr_td.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "FromScratch = yes\n",
    "UnitsOutput = eV_angstrom\n",
    "\n",
    "Radius = 3.5*angstrom\n",
    "Spacing = 0.18*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDTimeStep = 0.0023/eV\n",
    "TDMaxSteps = 4350  # ~ 10.0/TDTimeStep\n",
    "\n",
    "TDDeltaStrength = 0.01/angstrom\n",
    "TDPolarizationDirection = 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mv td.general/multipoles td.general/multipoles.3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Output\n",
    "The output should be very similar to the one from the [time-dependent propagation tutorial](../1-basics/6-TD_propagation.ipynb). The main difference is the information about the perturbation:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 2 \"Applying delta kick\" stdout_td.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 20 td.general/multipoles.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note how the dipole along the ''x'' direction (forth column) changes in response to the perturbation.\n",
    "\n",
    "##  Optical spectra\n",
    "\n",
    "In order to obtain the spectrum for a general system one would need to perform three time-dependent runs, each for a perturbation along a different Cartesian direction (''x'', ''y'', and ''z''). In practice this is what you would need to do:\n",
    "- Set the direction of the perturbation along ''x'' ([TDPolarizationDirection](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationdirection) = 1),\n",
    "- Run the time-propagation,\n",
    "- Rename the ```td.general/multipoles``` file to ```td.general/multipoles.1```,\n",
    "- Repeat the above step for directions ''y'' ([TDPolarizationDirection](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationdirection)) and ''z'' ([TDPolarizationDirection](https://www.octopus-code.org/documentation//13/variables/time-dependent/response/dipole/tdpolarizationdirection) = 3) to obtain files ```td.general/multipoles.2``` and ```td.general/multipoles.3```.\n",
    "\n",
    "Nevertheless, the $T_d$ symmetry of methane means that the response is identical for all directions and the absorption spectrum for ''x''-polarization will in fact be equivalent to the spectrum averaged over the three directions. You can perform the calculations for the ''y'' and ''z'' directions if you need to convince yourself that they are indeed equivalent, but you this will not be necessary to complete this tutorial.\n",
    "\n",
    "Note that ```octopus``` can actually use the knowledge of the symmetries of the system when calculating the spectrum. However, this is fairly complicated to understand for the purposes of this introductory tutorial, so it will be covered in the [Use of symmetries in optical spectra from time-propagation tutorial](../2-Optical-Response/6_Use_of_symmetries_in_optical_spectra_from_time_propagation.ipynb).\n",
    "\n",
    "### The ```oct-propagation_spectrum``` utility\n",
    "\n",
    "```octopus``` provides an utility called [oct-propagation_spectrum](https://www.octopus-code.org/documentation//13/manual/utilities/oct-propagation_spectrum) to process the ```multipoles``` files and obtain the spectrum.\n",
    "\n",
    "#### Input\n",
    "This utility requires little information from the input file, as most of what it needs is provided in the header of the ```multipoles``` files. If you want you can reuse the same input file as for the time-propagation run, but the following input file will also work:\n",
    "\n",
    "```\n",
    "%%writefele inp\n",
    "UnitsOutput = eV_angstrom\n",
    "```\n",
    "\n",
    "Don't worry about the warnings generated, we know what we are doing!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-propagation_spectrum"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we used the full input file, we see some more parser warnings, which indicate the unused variables.\n",
    "Here, these arise as the utility does not need them, but warnings like this also could indicate a mis-typed variable name.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_td.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will notice that the file ```cross_section_vector``` is created. If you have all the required information (either a symmetric molecule and one multipole file, or a less symmetric molecule and multiple multipole files), ```oct-propagation_spectrum``` will generate the whole polarizability tensor, ```cross_section_tensor```. If not enough multipole files are available, it can only generate the response to the particular polarization you chose in you time-dependent run, ```cross_section_vector```.\n",
    "\n",
    "### Cross-section vector\n",
    "\n",
    "Let us look first at ```cross_section_vector```:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 12 cross_section_vector.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The beginning of the file just repeats some information concerning the run.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -B 1 -A 9 \"#%\" cross_section_vector.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now comes a summary of the variables used to calculate the spectra. Note that you can change all these settings just by adding these variables to the input file before running ```oct-propagation_spectrum```.  Of special importance are perhaps [PropagationSpectrumMaxEnergy](https://www.octopus-code.org/documentation//13/variables/utilities/oct-propagation_spectrum/propagationspectrummaxenergy) that sets the maximum energy that will be calculated, and [PropagationSpectrumEnergyStep](https://www.octopus-code.org/documentation//13/variables/utilities/oct-propagation_spectrum/propagationspectrumenergystep) that determines how many points your spectrum will contain. To have smoother curves, you should reduce this last variable.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -B 2 -A 2 \"Electronic sum rule\" cross_section_vector.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now comes some information from the sum rules. The first is just the ''f''-sum rule, which should yield the number of active electrons in the calculations. We have 8 valence electrons (4 from carbon and 4 from hydrogen), but the sum rule gives a number that is much smaller than 8! The reason is that we are just summing our spectrum up to 20 eV (see [PropagationSpectrumMaxEnergy](https://www.octopus-code.org/documentation//13/variables/utilities/oct-propagation_spectrum/propagationspectrummaxenergy), but for the sum rule to be fulfilled, we should go to infinity. Of course infinity is a bit too large, but increasing 20 eV to a somewhat larger number will improve dramatically the ''f''-sum rule. The second number is the static polarizability also calculated from the sum rule. Again, do not forget to converge this number with [PropagationSpectrumEnergyStep](https://www.octopus-code.org/documentation//13/variables/utilities/oct-propagation_spectrum/propagationspectrumenergystep).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv(\n",
    "    \"cross_section_vector.1\",\n",
    "    delimiter=\"     \",\n",
    "    usecols=[0, 4],\n",
    "    names=[\"Energy (eV)\", \"StrengthFunction\"],\n",
    "    skiprows=26,\n",
    "    engine=\"python\",\n",
    ")\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally comes the spectrum. The first column is the energy (frequency), the next three columns are a row of the cross-section tensor, and the last one is the strength function for this run.\n",
    "\n",
    "The dynamic polarizability is related to optical absorption cross-section via $\\sigma \\left( \\omega \\right) = \\frac{4 \\pi \\omega}{c} \\mathrm{Im}\\ \\alpha \\left( \\omega \\right) $ in atomic units, or more generally $4 \\pi \\omega \\tilde{\\alpha}\\ \\mathrm{Im}\\ \\alpha \\left( \\omega \\right) $ (where $\\tilde{\\alpha}$ is the fine-structure constant) or $\\frac{\\omega e^2}{\\epsilon_0 c} \\mathrm{Im}\\ \\alpha \\left( \\omega \\right) $. The cross-section is related to the strength function by $S \\left( \\omega \\right) = \\frac{mc}{2 \\pi^2 \\hbar^2} \\sigma \\left( \\omega \\right)$.\n",
    "\n",
    "## Cross-section tensor\n",
    "\n",
    "If all three directions were done, we would have four files: ```cross_section_vector_1``` , ```cross_section_vector_2``` , ```cross_section_vector_3``` , and ```cross_section_tensor``` . The latter would be similar to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 20 cross_section_tensor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.plot(\n",
    "    x=\"Energy (eV)\",\n",
    "    y=\"StrengthFunction\",\n",
    "    legend=False,\n",
    "    title=\"Absorption spectrum of methane\",\n",
    "    ylabel=\"Strength function (1/eV)\",\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "The columns are now the energy, the average absorption coefficient (Tr $\\sigma/3$), the anisotropy, and\n",
    "then all the 9 components of the tensor. The third number is the spin component, just 1 here since it is unpolarized. The anisotropy is defined as\n",
    "\n",
    "$\n",
    "  (\\Delta \\sigma)^2 = \\frac{1}{3} \\{ 3\\rm Tr (\\sigma ^2) - \\rm [Tr(\\sigma)]^2 \\}\n",
    "$\n",
    "\n",
    "The reason for this definition is that it is identically equal to:\n",
    "\n",
    "$\n",
    " (\\Delta \\sigma)^2 = (1/3) [ (\\sigma_1-\\sigma_2)^2 + (\\sigma_1-\\sigma_3)^2 + (\\sigma_2-\\sigma_3)^2 ],\n",
    "$\n",
    "\n",
    "where $\\{\\sigma_1, \\sigma_2, \\sigma_3\\}\\,$ are the eigenvalues of $\\sigma\\,$. An \"isotropic\" tensor is characterized by having three equal eigenvalues, which leads to zero anisotropy. The more different that the eigenvalues are, the larger the anisotropy is.\n",
    "\n",
    "If you now plot the absorption spectrum (column 5 vs 1 in ```cross_section_vector```, but you can use ```cross_section_tensor``` for this exercise in case you did propagate in all three directions), you should obtain the plot shown above. Of course, you should now try to converge this spectrum with respect to the calculation parameters. In particular:\n",
    "\n",
    "* Increasing the total propagation time will reduce the width of the peaks. In fact, the width of the peaks in this methods is absolutely artificial, and is inversely proportional to the total propagation time. Do not forget, however, that the area under the peaks has a physical meaning: it is the oscillator strength of the transition.\n",
    "* A convergence study with respect to the spacing and box size might be necessary. This is covered in the next tutorial.\n",
    "\n",
    "Some questions to think about:\n",
    "* What is the equation for the peak width in terms of the propagation time? How does the observed width compare to your expectation?\n",
    "* What is the highest energy excitation obtainable with the calculation parameters here? Which is the key one controlling the maximum energy?\n",
    "* Why does the spectrum go below zero? Is this physical? What calculation parameters might change this?\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "[Go to *2_Convergence_of_optical_spectra.ipynb*](2_Convergence_of_optical_spectra.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
