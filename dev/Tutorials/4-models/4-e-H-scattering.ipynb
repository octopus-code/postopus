{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# e-H scattering\n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/13/tutorial/model/e-h_scattering/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "In this tutorial, we will show how to model the problem of an electron wavepacket scattering on an hydrogen atom using Octopus. In order to speed up the calculations and make it easier to plot the different quantities of interest, we will do the calculation in 1D. It should be straightforward to change this example to simulate a “real” 3D hydrogen atom."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "from postopus import Run\n",
    "import holoviews as hv\n",
    "from holoviews import opts\n",
    "\n",
    "hv.extension(\"bokeh\", \"matplotlib\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 4-e-H-scattering"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 4-e-H-scattering"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "## Ground state\n",
    "\n",
    "The first thing to do is to tell Octopus what we want it to do. We will start with an input file containing only the description of a 1D hydrogen atom, modeled by a soft Coulomb potential:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "FromScratch = yes\n",
    "CalculationMode = gs\n",
    "\n",
    "Dimensions = 1\n",
    "\n",
    "Radius = 50\n",
    "Spacing = 0.15\n",
    "\n",
    "%Species\n",
    " \"H\" | species_soft_coulomb | softening | 1 | valence | 1\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    " \"H\" | -10\n",
    "%\n",
    "\n",
    "SpinComponents = polarized\n",
    "\n",
    "ExtraStates = 1\n",
    "\n",
    "%Occupations\n",
    " 1 | 0\n",
    " 0 | 0\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "These input variables should already be familiar as they have been explained in previous tutorials. Here we simply note the following:\n",
    "\n",
    "* [Spacing](https://www.octopus-code.org/documentation//13/variables/mesh/spacing): We employed a large box. This is needed for the time-dependent simulation, and, as we will see, is in fact not large enough for correctly describing our e-H problem for even 1 fs.\n",
    "* [ExtraStates](https://www.octopus-code.org/documentation//13/variables/states/extrastates): We requested one extra state. This is not really needed for the ground-state calculation, but is crucial for the time-dependent run bellow, as there we will replace this unoccupied state by the electron wavepacket.\n",
    "* [Occupations](https://www.octopus-code.org/documentation//13/variables/states/occupations): We have fixed the occupations to be one in the first spin channel. This is needed for the TD run, as we will see, and also because otherwise the code might get to a different ground state than the one we are interested."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "## Output\n",
    "\n",
    "Now one can execute this file by running **Octopus**. Here are a few things worthy of a closer look in the standard output. We are doing an LDA calculation, as we want to investigate the role of electron-electron interaction here, and, as a possible application, benchmark the performance of exchange-correlation functionals. As we are doing a one dimensional calculation, we see that **Octopus** has selected the corresponding 1D version of the LDA functional:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_gs.txt | grep -A 15 \"[*] Theory Level [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "## e-H-scattering"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "We are now ready to perform a simulation of the e-H scattering problem. For this, we use the following input file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_td.txt'\n",
    "stderr = 'stderr_td.txt'\n",
    "FromScratch = yes\n",
    "CalculationMode = td\n",
    "\n",
    "Dimensions = 1\n",
    "\n",
    "Radius = 50\n",
    "Spacing = 0.15\n",
    "\n",
    "%Species\n",
    "\"H\" | species_soft_coulomb | softening | 1 | valence | 1\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    "\"H\" | -10\n",
    "%\n",
    "\n",
    "SpinComponents = polarized\n",
    "\n",
    "ExtraStates = 1\n",
    "\n",
    "ExcessCharge = -1\n",
    "%Occupations\n",
    " 1 | 1\n",
    " 0 | 0\n",
    "%\n",
    "RestartFixedOccupations = no\n",
    "\n",
    "a = 0.1\n",
    "x0 = 10.0\n",
    "p = -1.5\n",
    "%UserDefinedStates\n",
    "  1 | 2 | 1 | formula | \"exp(-a*(x-x0)^2+i*p*(x-x0))\"\n",
    "%\n",
    "\n",
    "%Output\n",
    " density\n",
    " potential\n",
    "%\n",
    "OutputFormat = axis_x\n",
    "TDPropagationTime = 0.8 * femtosecond"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "In order to introduce the electron wavepacket to scatter on the hydrogen atom, we have done the following change:\n",
    "* [ExcessCharge](https://www.octopus-code.org/documentation//13/variables/states/excesscharge): We introduce an extra electron in our simulation by setting this variable to -1.\n",
    "* [Occupations](https://www.octopus-code.org/documentation//13/variables/states/occupations): We force the extra electron to be on the second state of the first spin channel.\n",
    "* [RestartFixedOccupations](https://www.octopus-code.org/documentation//13/variables/states/restartfixedoccupations)\"  = no: Note, that we need to override the default behaviour to keep the occupations from the restart file.\n",
    "* [UserDefinedStates](https://www.octopus-code.org/documentation//13/variables/states/userdefinedstates): We are replacing the originally unoccupied state obtained after the GS calculation by a user defined state, which is a Gaussian of the form $\n",
    "\\phi(x) = e^{-\\alpha (x-x_0)^2+i p (x-x_0)}, $ where $\\alpha$ is taken to be $\\alpha=0.1$ here, $x_0$ is the initial position of the wavepacket, taken to be at $x_0=-10$ a.u., and $p$ its velocity, taken to be $p=-1.5$ a.u..\n",
    "\n",
    "\n",
    "The result of the calculation is shown in Fig. 1. What we are interested in is the density of the up channel, where both electrons are, as well as the corresponding potential. These quantities are found in the file 'output_iter/td.XXXXXXX/density-sp1.y=0,z=0' and  'output_iter/td.XXXXXXX/vxc-sp1.y=0,z=0'.\n",
    "We note a few interesting details. First, we observe no significant reflection of the wavepacket density on the hydrogen atom. This is a known deficiency of the adiabatic approximation.\n",
    "Moreover, towards the end of the simulation, both the density and the exchange-correlation potential show strong oscillations on the left-hand side of the simulation.\n",
    "This is due to the artificial reflection of the electron wavepacket at the border of the simulation box.\n",
    "This can be easily fixed by double the size of the box ([Radius](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/radius) = 100), and/or by using absorbing boundaries."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_td.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "## Create plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "nx = run.default.td.density_sp1()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "hv_nx = hv.Dataset(nx)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21",
   "metadata": {},
   "outputs": [],
   "source": [
    "vxc = run.default.td.vxc_sp1()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "hv_vxc = hv.Dataset(vxc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "layout = (\n",
    "    hv_nx.to(hv.Curve, \"x\", \"density_sp1\") + hv_vxc.to(hv.Curve, \"x\", \"vxc_sp1\")\n",
    ").cols(1)\n",
    "layout.opts(opts.Curve(width=450, height=450, framewise=True))\n",
    "layout"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "[Go to *5-Kronig-Penney-Model.ipynb*](5-Kronig-Penney-Model.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
