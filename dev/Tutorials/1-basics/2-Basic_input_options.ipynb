{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic input options\n",
    "[Link to tutorial](https://octopus-code.org/documentation/13/tutorial/basics/basic_input_options/)\n",
    "\n",
    "Now we will move to a more complicated (and realistic) input file. We will obtain the ground state of the nitrogen atom. We will introduce several basic input variables and will give a more detailed description of the output for this example.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 2-basic_input_options"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 2-basic_input_options"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generating the input file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This sample input file lets us obtain the ground state of the nitrogen atom, within the LDA approximation, in a closed-shell (unpolarized) configuration (as explained below, you need an auxiliary `.xyz` input). Note that this is not the correct ground state of the nitrogen atom! However, it will permit us to describe some of the most important input variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = \"stdout_gs.txt\"\n",
    "stderr = \"stderr_gs.txt\"\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_Angstrom\n",
    "\n",
    "Nitrogen_mass = 14.0\n",
    "\n",
    "%Species\n",
    " 'N' | species_pseudo | set | standard | lmax | 1 | lloc | 0 | mass | Nitrogen_mass\n",
    "%\n",
    "\n",
    "XYZCoordinates = 'N.xyz'\n",
    "\n",
    "ExtraStates = 1\n",
    "%Occupations\n",
    "  2 | 1 | 1 | 1\n",
    "  %\n",
    "\n",
    "BoxShape = sphere\n",
    "Radius = 5.0*angstrom\n",
    "Spacing = 0.18*angstrom"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have introduced here several new variables:\n",
    "\n",
    "* <code><a href=\"https://www.octopus-code.org/documentation//13/variables/execution/units/unitsoutput\">UnitsOutput</a> = eV_Angstrom</code>\n",
    ": Two different unit systems may be used for output: the usual atomic units (which is the default, and the ones used internally in the code); and the system in which the Ångström is substituted for the atomic unit of length, and the electronvolt is substituted for the atomic unit of energy. You can find a more detailed description of units in **Octopus** in the [Units](https://www.octopus-code.org/documentation/13/manual/basics/units/) page of the manual.\n",
    "\n",
    "* The following entry in the input file is not a variable that **Octopus** will read directly, but rather illustrates the possibility of writing \"user-defined\" values and expressions to simplify the input file. In this case, we define the nitrogen  mass (`Nitrogen_mass = 14.0`) (note that in this case, as an exception, the value is expected to be in the so-called \"atomic mass units\", rather than in \"atomic units\"). This definition may be used elsewhere in the input file.\n",
    "\n",
    "* The <code><a href=\"https://www.octopus-code.org/documentation//13/variables/execution/units/unitsoutput\">UnitsOutput</a></code> block should contain the list of species that are present in the system to be studied. In this case we have only one species: nitrogen. The first field is a string that defines the name of the species, \"N\" in this case. The second field defines the type of species, in this case  `species_pseudo`. Then a list of parameters follows. The parameters are specified by a first field with the parameter name and the field that follows with the value of the parameter. Some parameters are specific to a certain species while others are accepted by all species. In our example `set`  instructs **Octopus** to use a pseudopotential for nitrogen from the `standard` set. This happens to be a Troullier-Martins pseudopotential defined in the `.xyz` file found in the directory `N.psf`. Then come maximum `lmax` - component of the pseudopotential to consider in the calculation, and the `lloc` - component to consider as local. Generally, you want to set the maximum ''l'' to the highest available in the pseudopotential and the local ''l'' equal to the maximum ''l''. Finally, the mass of the species can also be modified from the default values by setting `mass` parameter.\n",
    "\n",
    "* <code><a href=\"https://www.octopus-code.org/documentation/13/variables/system/coordinates/xyzcoordinates/\">XYZCoordinates</a>= 'N.xyz'</code>: The geometry of the molecule (in this case, a single atom in the grid origin) is described in this case in a file with the well known `XYZ` format. The file for this outrageously simple case is given by:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile N.xyz\n",
    "\n",
    "\n",
    "1\n",
    "This is a comment line\n",
    "N 0 0 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* <code><a href=\"https://www.octopus-code.org/documentation//13/variables/states/extrastates\">ExtraStates</a>= 1</code>\n",
    ": By default, **Octopus** performs spin-unpolarized calculations (restricted closed-shell, in Hartree-Fock terminology). It then places two electrons in each orbital. The number of orbitals, or Kohn-Sham states, is then calculated by counting the number of valence electrons present in the system, and dividing by two. In this case, since we have five valence electrons, the code would use three orbitals. However, we know beforehand that the HOMO orbital has a three-fold degeneracy, and as a consequence we need to put each one of the three _p_ electrons in a different orbital. We therefore need one more orbital, which we get with this line in the input file.\n",
    "\n",
    "* <code>%<a href=https://www.octopus-code.org/documentation//13/variables/states/occupations>Occupations</a></code> block: Generally, the occupations of the Kohn-Sham orbitals are automatically decided by the code, filling the lowest-energy orbitals. However, if we have degeneracies in the LUMO as in this case, the user may want to accommodate the electrons in a certain predefined way. In this example, the obvious way to fill the orbitals of the nitrogen atom is to put two electrons in the first and deepest orbital (the _s_ orbital), and then one electron on each of the second, third and fourth orbitals (the _p_ orbitals, which should be degenerate).\n",
    "\n",
    "* <code><a href=https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/boxshape>BoxShape</a> = sphere</code>: This is the choice of the shape of the simulation box, which in this case is set to be a sphere (other possible choices are `Nitrogen_mass = 14.0`, `species_pseudo`, or `set`).\n",
    "\n",
    "* <code><a href=https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/radius>Radius</a> = 5.0*angstrom</code>: The radius of the sphere that defines the simulation box.\n",
    "\n",
    "* <code><a href=https://www.octopus-code.org/documentation//13/variables/mesh/spacing>Spacing</a> = 0.18*angstrom</code>: As you should know, **Octopus** works in a real-space regular cubic mesh. This variable defines the spacing between points, a key numerical parameter, in some ways equivalent to the energy cutoff in plane-wave calculations.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Output\n",
    "Once you have constructed the input file and created the `.xyz` file, you may unleash **Octopus** on it. Lets now go over some of the sections of the output.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Species"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 17 \"Species\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here the code searches for the needed pseudopotential files, and informs the user about its success or failure. In this case, only the `.xyz` file is required. Once that file has been processed, some information about it is written to the output. One of the most important pieces of information to be found here is the valence charge, which tells us how many electrons from this species will be considered in the calculation.\n",
    "\n",
    "#### Grid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 9 \" Grid\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This step is about the construction of the mesh. As requested in the input file, a sphere of radius 5 Å is used, which contains a cubic regular real-space grid with spacing 0.18 Å. This implies 89727 points (`Nitrogen_mass = 14.0`). For the sake of comparison with plane-wave-based codes, this is more or less equivalent to a plane-wave calculation that imposes a density cutoff of 1160.595 eV = 42.6 Hartree (except that in this case there is no artificial periodic repetition of the system).\n",
    "\n",
    "\n",
    "#### Mixing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 1 \"Mix\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "During the self-consistent procedure one has to use a [mixing scheme](https://octopus-code.org/documentation/13/manual/calculations/ground_state/#mixing) to help convergence. One can mix either the density or the potential, and there are several mixing schemes available.\n",
    "\n",
    "####  Eigensolver  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 6 \"Eigensolver\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we see that the [eigensolver](https://octopus-code.org/documentation/13/manual/calculations/ground_state/#eigensolver) used will be simple conjugate gradients (cg), and a preconditioner is used to speed up its convergence.\n",
    "\n",
    "####  LCAO  \n",
    "After some output you should see something like:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 8 \"initial LCAO calculation\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the first step of a ground-state calculation: obtaining a reasonably good starting density and Kohn-Sham orbitals to feed in the self-consistent (SCF) procedure. For this purpose, **Octopus** performs an initial calculation restricted to the basis set of atomic orbitals ( [Linear Combination of Atomic Orbitals](https://octopus-code.org/documentation/13/manual/calculations/ground_state/#lcao), LCAO). The resulting eigenvalues of this calculation are written to standard output.\n",
    "\n",
    "####  Wavefunction kind  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 0 \"real wavefunctions.\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Very often one can work with real wave-functions. This is particularly helpful as calculations with real wave-functions are much faster than with complex ones. However, if a magnetic field is present, if the system is periodic, or if spin-orbit coupling is present, complex wave-functions are mandatory. But don't worry: the program is able to figure out by itself what to use.\n",
    "\n",
    "####  SCF  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 10 \"SCF CYCLE\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the SCF cycle starts. For every step, **Octopus** outputs several pieces of information:\n",
    "\n",
    "* The values `Nitrogen_mass = 14.0` and `species_pseudo` are to monitor the absolute and relative convergence of the density, while `set` and `standard` are two alternative measures of the convergence, based on measuring the difference between input and output eigenvalues. The SCF procedure, by default, is stopped when `lmax` is smaller than $10^{-5}$. This may be altered with the appropriate input variables (see in the manual the variables [ConvAbsDens](https://www.octopus-code.org/documentation//13/variables/scf/convergence/convabsdens), [ConvRelDens](https://www.octopus-code.org/documentation//13/variables/scf/convergence/convreldens), [ConvAbsEv](https://www.octopus-code.org/documentation//13/variables/scf/convergence/convabsev) and [ConvRelEv](https://www.octopus-code.org/documentation//13/variables/scf/convergence/convrelev)).\n",
    "\n",
    "* The line `lloc` tells us that the Hamiltonian was applied 108 times. This gives us an idea of the computational cost.\n",
    "\n",
    "* The line `mass` tells us that upon completion of the diagonalization procedure, none of the orbitals met the required precision criterion for the wavefunctions. In a following example, we will modify this criterion in the input file.\n",
    "\n",
    "* The list of eigenvalues is then printed, along with their errors: how much they deviate from \"exact\" eigenvalues of the current Hamiltonian. This number is the so-called \"residue\".\n",
    "\n",
    "You can now take a look at the file `.xyz` that will hold a summary of the calculation.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat static/info"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Restarting\n",
    "Any ground-state calculation may be restarted later (to refine it if it did not converge properly, or with any other purpose), provided that the contents of the `restart` directory are preserved. You can try this now, just by running **Octopus** again. You will notice that **Octopus** did not give any warning.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = \"stdout_gs_restart.txt\"\n",
    "stderr = \"stderr_gs_restart.txt\"\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_Angstrom\n",
    "\n",
    "Nitrogen_mass = 14.0\n",
    "\n",
    "%Species\n",
    " 'N' | species_pseudo | set | standard | lmax | 1 | lloc | 0 | mass | Nitrogen_mass\n",
    "%\n",
    "\n",
    "XYZCoordinates = 'N.xyz'\n",
    "\n",
    "ExtraStates = 1\n",
    "%Occupations\n",
    "  2 | 1 | 1 | 1\n",
    "  %\n",
    "\n",
    "BoxShape = sphere\n",
    "Radius = 5.0*angstrom\n",
    "Spacing = 0.18*angstrom"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!cat \"stdout_gs_restart.txt\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1  \"restart\" \"stdout_gs_restart.txt\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second run is shorter as it uses the restart files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1  \"Walltime\" \"stdout_gs.txt\"\n",
    "!grep -am 1  \"Walltime\" \"stdout_gs_restart.txt\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is useful if you change slightly the parameters of the simulation (for example the XC functional or the convergence criteria). If you change the grid parameters **Octopus** will not be able to restart from the previous calculation. If you do not want **Octopus** to try to restart a calculation, you can set the variable [FromScratch](https://www.octopus-code.org/documentation//13/variables/execution/fromscratch).\n",
    "\n",
    "In case you were wondering what the restart information looks like, you can have a look at the contents of the `.xyz` directory. This is where the files needed to restart a calculation are stored. It may contain several sub-directories depending on the calculations previously performed. In this case, it just contains one:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls restart/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls restart/gs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Octopus** stores each individual state in a different binary (yet platform-independent) file. In this case, we only have four states (files `.xyz` to `N.psf`). Some other useful quantities, like the density, are also stored in binary form. The other files are text files that contain diverse control information. It is unlikely that you will ever have to work directly with these files, but you may take a look around if you are curious. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Go to *3-Total_energy_convergence.ipynb*](3-Total_energy_convergence.ipynb)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  },
  "vscode": {
   "interpreter": {
    "hash": "b71578c3734d080eaa56fa8979e33f06feb9f2966868d8c6a4b5b7c486c8b512"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
