{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Band structure unfolding\n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/13/tutorial/periodic_systems/unfolding/)\n",
    "\n",
    "In this tutorial, we look at how to perform a band-structure unfolding using **Octopus**. This calculation is done in several steps and each one is described below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from scipy.interpolate import griddata"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir 4-Band-structure-unfolding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 4-Band-structure-unfolding/"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Supercell ground-state\n",
    "The first thing to do is to compute the ground state of a supercell. In this example we will use bulk silicon. The input file is similar to the one used in the [Getting started with periodic systems](1-getting-started.ipynb) tutorial, but in the present case we will use a supercell composed of 8 atoms. Here is the corresponding input file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a  | a\n",
    "90 | 90 |90\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0         | 0.0       | 0.0\n",
    " \"Si\" |   1/2       | 1/2       | 0.0\n",
    " \"Si\" |   1/2       | 0.0       | 1/2\n",
    " \"Si\" |   0.0       | 1/2       | 1/2\n",
    " \"Si\" |   1/4       | 1/4       | 1/4\n",
    " \"Si\" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4\n",
    " \"Si\" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2\n",
    " \"Si\" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2\n",
    "%\n",
    "\n",
    "nk = 4\n",
    "%KPointsGrid\n",
    "  nk |  nk |  nk\n",
    "%\n",
    "KPointsUseSymmetries = yes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "All these variables should be familiar from other tutorials. Now run **Octopus** using this input file to obtain the ground-state of the supercell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "## Unfolding setup\n",
    "\n",
    "After obtaining the ground state of the supercell, we now define the primitive cell on which we want to unfold our supercell and the specific ‘‘k’'-points path that we are interested in. To do this, we take the previous input file and add some new lines:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "CalculationMode = gs\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a  | a\n",
    "90 | 90 |90\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0         | 0.0       | 0.0\n",
    " \"Si\" |   1/2       | 1/2       | 0.0\n",
    " \"Si\" |   1/2       | 0.0       | 1/2\n",
    " \"Si\" |   0.0       | 1/2       | 1/2\n",
    " \"Si\" |   1/4       | 1/4       | 1/4\n",
    " \"Si\" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4\n",
    " \"Si\" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2\n",
    " \"Si\" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2\n",
    "%\n",
    "\n",
    "nk = 4\n",
    "%KPointsGrid\n",
    "  nk |  nk |  nk\n",
    "%\n",
    "KPointsUseSymmetries = yes\n",
    "\n",
    "ExperimentalFeatures = yes\n",
    "UnfoldMode = unfold_setup\n",
    "%UnfoldLatticeParameters\n",
    "  a | a | a\n",
    "%\n",
    "%UnfoldLatticeVectors\n",
    " 0.  | 0.5 | 0.5\n",
    " 0.5 | 0.  | 0.5\n",
    " 0.5 | 0.5 | 0.0\n",
    "%\n",
    "%UnfoldKPointsPath\n",
    " 4 | 4 | 8\n",
    " 0.5 | 0.0 | 0.0 # L point\n",
    " 0.0 | 0.0 | 0.0 # Gamma point\n",
    " 0.0 | 0.5 | 0.5 # X point\n",
    " 1.0 | 1.0 | 1.0 # Another Gamma point\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "Lets see more in detail the input variables that were added:\n",
    "\n",
    "* [UnfoldMode](https://www.octopus-code.org/documentation//13/variables/utilities/oct-unfold/unfoldmode) = unfold_setup: this variable instructs the utility oct-unfold in which mode we are running. As a first step, we are running in the unfold_setup mode, which generates some files that will be necessary for the next steps.\n",
    "\n",
    "* [UnfoldLatticeParameters](https://www.octopus-code.org/documentation//13/variables/utilities/oct-unfold/unfoldlatticeparameters) specifies the lattice parameters of the primitive cell. This variable is similar to [LatticeParameters](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/latticeparameters).\n",
    "\n",
    "* [UnfoldLatticeVectors](https://www.octopus-code.org/documentation//13/variables/utilities/oct-unfold/unfoldlatticevectors) specifies the lattice vectors of the primitive cell. This variable is similar to [LatticeVectors](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/latticevectors).\n",
    "\n",
    "* [UnfoldKPointsPath](https://www.octopus-code.org/documentation//13/variables/utilities/oct-unfold/unfoldkpointspath) specifies the ‘‘k’'-points path. The coordinates are indicated as reduced coordinates of the primitive lattice. This variable is similar to [KPointsPath](https://www.octopus-code.org/documentation//13/variables/mesh/kpoints/kpointspath)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "Now run the oct-unfold utility. You will obtain two files. The first one (unfold_kpt.dat ) contains the list of ‘‘k’'-points of the specified ‘‘k’'-point path, but expressed in reduced coordinates of the supercell. These are the ‘‘k’'-points for which we need to evaluate the wavefunctions of the supercell. The second file (unfold_gvec.dat ) contains the list of reciprocal lattice vectors that relate the ‘‘k’'-points of the primitive cell to the one in the supercell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-unfold"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "## Obtaining the wavefunctions\n",
    "\n",
    "In order to perform the unfolding, we need the wavefunctions in the supercell at specific k-points. These points are described in the unfold_kpt.dat file that we obtained in the previous step. To get the wavefunctions, we need to run a non self-consistent calculation. Here is the corresponding input file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_wf.txt'\n",
    "stderr = 'stderr_wf.txt'\n",
    "\n",
    "CalculationMode = unocc\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a  | a\n",
    "90 | 90 |90\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0         | 0.0       | 0.0\n",
    " \"Si\" |   1/2       | 1/2       | 0.0\n",
    " \"Si\" |   1/2       | 0.0       | 1/2\n",
    " \"Si\" |   0.0       | 1/2       | 1/2\n",
    " \"Si\" |   1/4       | 1/4       | 1/4\n",
    " \"Si\" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4\n",
    " \"Si\" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2\n",
    " \"Si\" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2\n",
    "%\n",
    "\n",
    "ExperimentalFeatures = yes\n",
    "UnfoldMode = unfold_setup\n",
    "%UnfoldLatticeParameters\n",
    "  a | a | a\n",
    "%\n",
    "%UnfoldLatticeVectors\n",
    " 0.  | 0.5 | 0.5\n",
    " 0.5 | 0.  | 0.5\n",
    " 0.5 | 0.5 | 0.0\n",
    "%\n",
    "%UnfoldKPointsPath\n",
    " 4 | 4 | 8\n",
    " 0.5 | 0.0 | 0.0 # L point\n",
    " 0.0 | 0.0 | 0.0 # Gamma point\n",
    " 0.0 | 0.5 | 0.5 # X point\n",
    " 1.0 | 1.0 | 1.0 # Another Gamma point\n",
    "%\n",
    "\n",
    "include unfold_kpt.dat\n",
    "\n",
    "ExtraStates = 6\n",
    "ExtraStatesToConverge = 4"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "In this input file we have changed the calculation mode ([CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) = unocc) and replaced all the ‘‘k’'-points related variables (%[KPointsGrid](https://www.octopus-code.org/documentation//13/variables/mesh/kpoints/kpointsgrid), %[KPointsPath](https://www.octopus-code.org/documentation//13/variables/mesh/kpoints/kpointspath), and %[KPoints](https://www.octopus-code.org/documentation//13/variables/mesh/kpoints/kpoints)) by the line:\n",
    "\n",
    "  include unfold_kpt.dat"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "When doing a normal band structure calculation one normally also wants to have some extra states, therefore we have used the [ExtraStates](https://www.octopus-code.org/documentation//13/variables/states/extrastates) and [ExtraStatesToConverge](https://www.octopus-code.org/documentation//13/variables/states/extrastatestoconverge) variables, as explained in the [Getting started with periodic systems](1-getting-started.ipynb) tutorial. In this particular case, we are requesting to converge 4 unoccupied states, which all correspond to the first valence band in the folded primitive cell, as the supercell is 4 times larger than the initial cell."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "Now run **Octopus** on this input file.\n",
    "\n",
    "<div style=\"border: 1px solid black; padding: 10px; background-color: #FFCCCC\">\n",
    "<span style=\"color: red;\"> Warning <br> Note that after you have performed this step, you won’t be able to start a time-dependent calculation from this folder. Indeed, the original ground-state wavefunctions will be replaced by the ones from this last calculation, which are incompatible. Therefore it might be a good idea to create a backup of the restart information before performing this step.</span>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "Alternatively, you can continue in a different directory. Instead of copying the restart information to the new folder, you can use the input variable [RestartOptions](https://www.octopus-code.org/documentation//13/variables/execution/io/restartoptions) to specify explicitely from which directory the restart files are to be read."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "## Unfolding"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "Now that we have computed the states for the required ‘‘k’'-points, we can finally compute the spectral function for each of the ‘‘k’'-points of the specified ‘‘k’'-point path. This is done by changing unfolding mode to be [UnfoldMode](https://www.octopus-code.org/documentation//13/variables/utilities/oct-unfold/unfoldmode) = unfold_run, so the final input file should look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp \n",
    "\n",
    "stdout = 'stdout_unfol.txt'\n",
    "stderr = 'stderr_unfol.txt'\n",
    "\n",
    "CalculationMode = unocc\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a  | a\n",
    "90 | 90 |90\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0         | 0.0       | 0.0\n",
    " \"Si\" |   1/2       | 1/2       | 0.0\n",
    " \"Si\" |   1/2       | 0.0       | 1/2\n",
    " \"Si\" |   0.0       | 1/2       | 1/2\n",
    " \"Si\" |   1/4       | 1/4       | 1/4\n",
    " \"Si\" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4\n",
    " \"Si\" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2\n",
    " \"Si\" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2\n",
    "%\n",
    "\n",
    "ExperimentalFeatures = yes\n",
    "UnfoldMode = unfold_run\n",
    "%UnfoldLatticeParameters\n",
    "  a | a | a\n",
    "%\n",
    "%UnfoldLatticeVectors\n",
    " 0.  | 0.5 | 0.5\n",
    " 0.5 | 0.  | 0.5\n",
    " 0.5 | 0.5 | 0.0\n",
    "%\n",
    "%UnfoldKPointsPath\n",
    " 4 | 4 | 8\n",
    " 0.5 | 0.0 | 0.0 # L point\n",
    " 0.0 | 0.0 | 0.0 # Gamma point\n",
    " 0.0 | 0.5 | 0.5 # X point\n",
    " 1.0 | 1.0 | 1.0 # Another Gamma point\n",
    "%\n",
    "\n",
    "include unfold_kpt.dat\n",
    "\n",
    "ExtraStates = 6\n",
    "ExtraStatesToConverge = 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-unfold"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "This will produce the spectral function for the full path (static/ake.dat) and for each individual points of the path (static/ake_XXX.dat). The content of the static/ake.dat file should look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat static/ake.dat"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26",
   "metadata": {},
   "source": [
    "The first column is the coordinate along the ‘‘k’'-point path, the second column is the energy eigenvalue and the last column is the spectral function. There are several ways to plot the information contained in this file. One possibility is to plot it as a heat map. For example, if you are using gnuplot, you can try the following command:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "The unfolded bandstructure of silicon is shown below. How does it compare to the bandstructure compute in the [Getting started with periodic systems](1-getting-started.ipynb)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = np.loadtxt(\"static/ake.dat\")\n",
    "\n",
    "x = data[:, 0]\n",
    "y = data[:, 1]\n",
    "intensity = data[:, 2]\n",
    "\n",
    "x_min, x_max = x.min(), x.max()\n",
    "y_min, y_max = y.min(), y.max()\n",
    "\n",
    "num_points = 100\n",
    "\n",
    "xi = np.linspace(x_min, x_max, num_points)\n",
    "yi = np.linspace(y_min, y_max, num_points)\n",
    "xi, yi = np.meshgrid(xi, yi)\n",
    "\n",
    "zi = griddata((x, y), intensity, (xi, yi), method=\"nearest\")\n",
    "\n",
    "plt.imshow(zi, extent=[x_min, x_max, y_min, y_max], origin=\"lower\", cmap=\"viridis\")\n",
    "\n",
    "plt.colorbar(label=\"Intensity\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29",
   "metadata": {},
   "source": [
    "Unfolded band structure of bulk silicon."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30",
   "metadata": {},
   "source": [
    "Note that it is possible to change the energy range and the energy resolution for the unfolded band structure. This is done by specifying the variables [UnfoldMinEnergy](https://www.octopus-code.org/documentation//13/variables/utilities/oct-unfold/unfoldminenergy), [UnfoldMaxEnergy](https://www.octopus-code.org/documentation//13/variables/utilities/oct-unfold/unfoldmaxenergy), and [UnfoldEnergyStep](https://www.octopus-code.org/documentation//13/variables/utilities/oct-unfold/unfoldenergystep). It is also important to note here that the code needs to read all the unoccupied wavefunctions and therefore might need a large amount of memory, so make sure enough memory is available."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "31",
   "metadata": {},
   "source": [
    "[Go to *5-Sternheimer.ipynb*](5-Sternheimer.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
