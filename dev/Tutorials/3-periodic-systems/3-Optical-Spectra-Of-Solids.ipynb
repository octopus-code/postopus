{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Optical Spectra Of Solids\n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/13/tutorial/periodic_systems/optical_spectra_of_solids/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "In this tutorial we will explore how to compute the optical properties of bulk systems from real-time TDDFT using **Octopus**. Two methods will be presented, one for computing the optical conductivity from the electronic current and another one computing directly the dielectric function using the so-called “gauge-field” kick method. Note that some of the calculations in this tutorial are quite heavy computationally, so they might require to run in parallel over a several cores to finish in a reasonable amount of time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "mkdir 3-Optical-Spectra-Of-Solids"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 3-Optical-Spectra-Of-Solids"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "### Groundstate"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "#### Input\n",
    "As always, we will start with the input file for the ground state, here of bulk silicon. In this case we will use a primitive cell of Si, composed of two atoms, as used in the [Getting started with periodic systems](1-getting-started.ipynb) tutorial:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "%LatticeVectors\n",
    " 0.0 | 0.5 | 0.5\n",
    " 0.5 | 0.0 | 0.5\n",
    " 0.5 | 0.5 | 0.0\n",
    "%\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a | a\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0 | 0.0 | 0.0\n",
    " \"Si\" | 1/4 | 1/4 | 1/4\n",
    "%\n",
    "\n",
    "nk = 2\n",
    "%KPointsGrid\n",
    " nk  | nk  | nk\n",
    " 0.5 | 0.5 | 0.5\n",
    " 0.5 | 0.0 | 0.0\n",
    " 0.0 | 0.5 | 0.0\n",
    " 0.0 | 0.0 | 0.5\n",
    "%\n",
    "KPointsUseSymmetries = yes\n",
    "%SymmetryBreakDir\n",
    " 1 | 0 | 0\n",
    "%\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "The description of most variables is given in the [Getting started with periodic systems](1-getting-started.ipynb). Here we only added one variable:\n",
    "\n",
    "* [SymmetryBreakDir](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/symmetrybreakdir): this input variable is used here to specify that we want to remove from the list of possible symmetries to be used the ones that are broken by a perturbation along the x axis. Indeed, we will later apply a perturbation along this direction, which will break the symmetries of the system."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "### Output \n",
    "\n",
    "Now run **Octopus** using the above input file. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "The important thing to note from the output is the effect of the [SymmetryBreakDir](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/symmetrybreakdir) input option:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_gs.txt | grep -A 11 \"[*] Symmetries [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "Here Octopus tells us that only 4 symmetries can be used, whereas without [SymmetryBreakDir](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/symmetrybreakdir), 24 symmetries could have been used. Since only some symmetries are used, twelve ‘‘k’'-points are generated, instead of two ‘‘k’'-points if we would have used the full list of symmetries. If we had not used symmetries at all, we would have 32 ‘‘k’'-points instead."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "### Optical conductivity\n",
    "\n",
    "We now turn our attention to the calculation of the optical conductivity. For this, we will apply to our system a time-dependent vector potential perturbation in the form of a Heaviside step function. This corresponds to applying a delta-kick electric field. However, because electric fields (length gauge) are not compatible with the periodicity of the system, we work here with vector potentials (velocity gauge).\n",
    "\n",
    "The corresponding input file is given below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_td.txt'\n",
    "stderr = 'stderr_td.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "FromScratch = yes\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "%LatticeVectors\n",
    "  0.0 | 0.5 | 0.5\n",
    "  0.5 | 0.0 | 0.5\n",
    "  0.5 | 0.5 | 0.0\n",
    "%\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a | a\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0 | 0.0 | 0.0\n",
    " \"Si\" | 1/4 | 1/4 | 1/4\n",
    "%\n",
    "\n",
    "nk = 2\n",
    "%KPointsGrid\n",
    "  nk |  nk |  nk\n",
    " 0.5 | 0.5 | 0.5\n",
    " 0.5 | 0.0 | 0.0\n",
    " 0.0 | 0.5 | 0.0\n",
    " 0.0 | 0.0 | 0.5\n",
    "%\n",
    "KPointsUseSymmetries = yes\n",
    "%SymmetryBreakDir\n",
    "  1 | 0 | 0\n",
    "%\n",
    "\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "%TDExternalFields\n",
    " vector_potential | 1.0 | 0.0 | 0.0 | 0.0 | \"envelope_step\"\n",
    "%\n",
    "\n",
    "%TDFunctions\n",
    " \"envelope_step\" | tdf_from_expr | \"0.01*step(t)\"\n",
    "%\n",
    "\n",
    "%TDOutput\n",
    " total_current\n",
    "%\n",
    "\n",
    "TDTimeStep = 0.3\n",
    "TDPropagationTime = 1500\n",
    "TDExponentialMethod = lanczos\n",
    "TDExpOrder = 16"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "Most variables have already been discussed in the different prior tutorials. Here are the most important considerations:\n",
    "\n",
    "* [TDOutput](https://www.octopus-code.org/documentation//13/variables/time-dependent/td_output/tdoutput): We ask for outputting the total electronic current. Without this, we will not be able to compute the optical conductivity.\n",
    "* [TDExternalFields:](https://www.octopus-code.org/documentation//13/variables/time-dependent/tdexternalfields) We applied a vector potential perturbation along the ‘‘x’’ axis using.\n",
    "* [TDFunctions](https://www.octopus-code.org/documentation//13/variables/time-dependent/tdfunctions): We defined a step function, an used a strength of the perturbation of 0.01. Note that the strength of the perturbation should be small enough, to guaranty that we remain in the linear response regime. Otherwise, nonlinear effects are coupling different frequencies and a an approach based of the “kick” method is not valid anymore.\n",
    "Note that if you would have forgotten above to specify [SymmetryBreakDir](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/symmetrybreakdir), the code could would have stopped and let you know that your laser field is breaking some of the symmetries of the system:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "******************************* FATAL ERROR ******************************* <br>\n",
    "  \\*\\* Fatal Error (description follows) <br>\n",
    "  \\* In namespace Lasers: <br>\n",
    "  \\*--------------------------------------------------------------------  <br>\n",
    "  \\* The lasers break (at least) one of the symmetries used to reduce the k-points  .  <br>\n",
    "  \\* Set SymmetryBreakDir accordingly to your laser fields.  <br>\n",
    "***********************************************************************"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "Now run **Octopus** using the above input file."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "The time requiered to create all runs are in the order of 8 minutes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "After the calculation is finished, you can find in the td.general folder the total_current file, that contains the computed electronic current. From it, we want to compute the optical conductivity. This is done by running the utility oct-conductivity . Before doing so, we need to specify the type of broadening that we want to apply to our optical spectrum. In real time, this correspond to applying a damping function to the time signal. For instance, the Lorentzian broadening that we will employ in this tutorial is given by an exponential damping. To specify it, simply add the line\n",
    "\n",
    "\n",
    "`PropagationSpectrumDampMode = exponential`\n",
    "\n",
    "to your input file before running the oct-conductivity utility. Running the utility produces the file td.general/conductivity , which contains the optical conductivity \n",
    "σ(ω) of our system. However, note that at the moment the obtained conductivity is not scaled properly, it is normalized to the volume and proportional to the strength of the perturbation. Hence, in order to obtain the absorption spectrum, we need to compute\n",
    "\n",
    "$ℑ[ϵ(ω)]= \\frac{4\\pi V}{E_0\\omega} ℜ[σ(ω)]$ \n",
    "\n",
    "where ***V*** is the volume of the system (in this case 10.18^3/4) and E_0 \n",
    "is the strength of the kick (here 0.01). Since our perturbation was \n",
    "along the ‘‘x’’ direction, we want to plot the ‘‘x’’ component of the conductivity. \n",
    "Therefore, in the end, we need to plot the second column of td.general/conductivity multiplied by \n",
    "appropriate factor versus the energy (first column). You can see how the spectrum should look like in Fig. 1. \n",
    "Because we are computing a current-current response, the spectrum exhibits a divergence at low frequencies. \n",
    "This numerical artifact vanishes when more ‘‘k’'-points are included in the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "CalculationMode = td\n",
    "FromScratch = yes\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "%LatticeVectors\n",
    "  0.0 | 0.5 | 0.5\n",
    "  0.5 | 0.0 | 0.5\n",
    "  0.5 | 0.5 | 0.0\n",
    "%\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a | a\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0 | 0.0 | 0.0\n",
    " \"Si\" | 1/4 | 1/4 | 1/4\n",
    "%\n",
    "\n",
    "nk = 2\n",
    "%KPointsGrid\n",
    "  nk |  nk |  nk\n",
    " 0.5 | 0.5 | 0.5\n",
    " 0.5 | 0.0 | 0.0\n",
    " 0.0 | 0.5 | 0.0\n",
    " 0.0 | 0.0 | 0.5\n",
    "%\n",
    "KPointsUseSymmetries = yes\n",
    "%SymmetryBreakDir\n",
    "  1 | 0 | 0\n",
    "%\n",
    "\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "%TDExternalFields\n",
    " vector_potential | 1.0 | 0.0 | 0.0 | 0.0 | \"envelope_step\"\n",
    "%\n",
    "\n",
    "%TDFunctions\n",
    " \"envelope_step\" | tdf_from_expr | \"0.01*step(t)\"\n",
    "%\n",
    "\n",
    "%TDOutput\n",
    " total_current\n",
    "%\n",
    "\n",
    "TDTimeStep = 0.3\n",
    "TDPropagationTime = 1500\n",
    "TDExponentialMethod = lanczos\n",
    "TDExpOrder = 16\n",
    "\n",
    "\n",
    "PropagationSpectrumDampMode = exponential"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-conductivity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24",
   "metadata": {},
   "outputs": [],
   "source": [
    "const = np.pi * 10.18**3 / 0.01"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25",
   "metadata": {},
   "source": [
    "Looking at the optical spectrum, we observe three main peaks. These correspond to the $E_0$, $E_1$, and $E_1$ critical points of the bandstructure of silicon. Of course, the shape of the spectrum is not converged because we used too few ‘‘k’'-points to sample the Brillouin zone. Remember that you should also perform a convergence study of the spectrum with respect with the relevant parameters. In this case those would be the spacing and the ‘‘k’'-points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26",
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv(\n",
    "    \"td.general/conductivity\",\n",
    "    skiprows=4,\n",
    "    usecols=[0, 1],\n",
    "    names=[\"energy\", \"conductivity\"],\n",
    "    engine=\"python\",\n",
    "    sep=\"\\s{4}|(?<=\\s)-\\s?\",\n",
    ")\n",
    "\n",
    "df[\"absorption\"] = const * df[\"conductivity\"].values / (df[\"energy\"].values + 10**-8)\n",
    "\n",
    "df.plot(x=\"energy\", y=\"absorption\", xlim=[0, 1], ylim=[-20, 200]);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "# Dielectric function using gauge field kick"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28",
   "metadata": {},
   "source": [
    "Let us now look at a different way to compute the dielectric function of a solid. The dielectric function can be defined from the link between the macroscopic total electric field (or vector potential) and the external electric field. Thus, one can solve the Maxwell equation for computing the macroscopic induced field in order to get the total vector potential acting on the system.\n",
    "\n",
    "This is the reasoning behind the gauge-field method, as proposed in Ref [$^1$](#first_reference).\n",
    "\n",
    "The corresponding input file is given below:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp \n",
    "\n",
    "stdout = 'stdout_die_gs.txt'\n",
    "stderr = 'stderr_die_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "%LatticeVectors\n",
    " 0.0 | 0.5 | 0.5\n",
    " 0.5 | 0.0 | 0.5\n",
    " 0.5 | 0.5 | 0.0\n",
    "%\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a | a\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0 | 0.0 | 0.0\n",
    " \"Si\" | 1/4 | 1/4 | 1/4\n",
    "%\n",
    "\n",
    "nk = 5\n",
    "%KPointsGrid\n",
    " nk  | nk  | nk\n",
    " 0.5 | 0.5 | 0.5\n",
    " 0.5 | 0.0 | 0.0\n",
    " 0.0 | 0.5 | 0.0\n",
    " 0.0 | 0.0 | 0.5\n",
    "%\n",
    "KPointsUseSymmetries = yes\n",
    "%SymmetryBreakDir\n",
    " 1 | 0 | 0\n",
    "%\n",
    "\n",
    "ExtraStates = 1\n",
    "%Output\n",
    " dos\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp \n",
    "\n",
    "stdout = 'stdout_die_td.txt'\n",
    "stderr = 'stderr_die_td.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "FromScratch = yes\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "%LatticeVectors\n",
    "  0.0 | 0.5 | 0.5\n",
    "  0.5 | 0.0 | 0.5\n",
    "  0.5 | 0.5 | 0.0\n",
    "%\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a | a\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0 | 0.0 | 0.0\n",
    " \"Si\" | 1/4 | 1/4 | 1/4\n",
    "%\n",
    "\n",
    "nk = 5\n",
    "%KPointsGrid\n",
    "  nk |  nk |  nk\n",
    " 0.5 | 0.5 | 0.5\n",
    " 0.5 | 0.0 | 0.0\n",
    " 0.0 | 0.5 | 0.0\n",
    " 0.0 | 0.0 | 0.5\n",
    "%\n",
    "KPointsUseSymmetries = yes\n",
    "%SymmetryBreakDir\n",
    "  1 | 0 | 0\n",
    "%\n",
    "\n",
    "%GaugeVectorField\n",
    " 0.1 | 0 | 0\n",
    "%\n",
    "\n",
    "TDTimeStep = 0.2\n",
    "TDPropagationTime = 1500\n",
    "TDExponentialMethod = lanczos\n",
    "TDExpOrder = 16"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32",
   "metadata": {},
   "source": [
    "The time requiered to execute the following run are at least in the order of 120 minutes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34",
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-dielectric-function"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35",
   "metadata": {},
   "source": [
    "Note that before running this input file, you need to redo a GS calculation setting ``nk = 5``, to have the same ‘‘k’'-point grid as in the above input file.\n",
    "\n",
    "Compared to the previous TD calculation, we have made few changes:\n",
    "\n",
    "* [GaugeVectorField](https://www.octopus-code.org/documentation//13/variables/hamiltonian/gaugevectorfield): This activates the calculation of the gauge field and its output. This replaces the laser field used in the previous method.\n",
    "* [KPointsGrid](https://www.octopus-code.org/documentation//13/variables/mesh/kpoints/kpointsgrid): We used here a much denser ‘‘k’'-point grid. This is needed because of the numerical instability of the gauge-field method for very low number of ‘‘k’'-points.\n",
    "\n",
    "After running this input file with **Octopus**, we can run the utility oct-dielectric-function , which produces the output td.general/dielectric_function . In this case we are interested in the imaginary part of the dielectric function along the ‘‘x’’ direction, which correspondents to the third column of the file. You can see how the spectra looks like in Fig. 2. As we can see, below the bandgap, we observe some numerical noise. This is again due to the fact that the spectrum is not properly converged with respect to the number of ‘‘k’'-points and vanishes if sufficient ‘‘k’'-points are used. Apart from this, we now obtained a much more converged spectrum, with the main three features starting to resemble the fully-converged spectrum."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36",
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv(\n",
    "    \"td.general/dielectric_function\",\n",
    "    usecols=[0, 2],\n",
    "    names=[\"energy\", \"absorption\"],\n",
    "    skiprows=1,\n",
    "    sep=\"\\s{3}|(?<=\\s)s?\",\n",
    "    engine=\"python\",\n",
    ")\n",
    "\n",
    "\n",
    "df[\"energy\"] = df[\"energy\"] * 27.2114\n",
    "\n",
    "\n",
    "df.plot(\n",
    "    x=\"energy\",\n",
    "    y=\"absorption\",\n",
    "    label=False,\n",
    "    ylim=[-500, 500],\n",
    "    title=\"Absorption spectrum of Si obtained using the gauge-field method.\",\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37",
   "metadata": {},
   "source": [
    "[Go to *4-Band-structure-unfolding.ipynb*](4-Band-structure-unfolding.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38",
   "metadata": {},
   "source": [
    "1. Bertsch, G. F. and Iwata, J.-I. and Rubio, Angel and Yabana, K., Real-space, real-time method for the dielectric function, [Phys. Rev. B](https://doi.org/10.1103/PhysRevB.62.7998) 62 7998 (2000);\n",
    "<span id=\"first_reference\"></span>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
