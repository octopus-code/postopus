{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Wires and slabes\n",
    "[Link to tutorial](https://www.octopus-code.org/documentation/13/tutorial/periodic_systems/wires_and_slabs/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "In this tutorial we will explain how to use the flexibility of the real-space grid to treat systems that are periodic in only one or two dimensions. As examples we will use a Na chain and a hexagonal boron nitride (h-BN) monolayer.\n",
    "\n",
    "Introduction \n",
    "\n",
    "In the [Periodic systems](https://www.octopus-code.org/documentation//13/tutorial/periodic_systems) tutorial, we saw that the [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions) input variable controls the number of dimensions to be considered as periodic. In that tutorial we only considered the case PeriodicDimensions = 3. Let’s now see in detail the different cases:\n",
    "\n",
    "* [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions) = 0 (which is the default) gives a finite system calculation, since Dirichlet zero boundary conditions are used at all the borders of the simulation box;\n",
    "\n",
    "* [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions) = 1 means that only the ‘‘x’’ axis is periodic, while in all the other directions the system is confined. This value must be used to simulate, for instance, a single infinite wire.\n",
    "\n",
    "* [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions) = 2 means that both ‘‘x’’ and ‘‘y’’ axis are periodic, while zero boundary conditions are imposed at the borders crossed by the ‘‘z’’ axis. This value must be used to simulate, for instance, a single infinite slab.\n",
    "\n",
    "* [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions) = 3 means that the simulation box is a primitive cell for a fully periodic infinite crystal. Periodic boundary conditions are imposed at all borders.\n",
    "\n",
    "It is important to understand that performing, for instance, a [PeriodicDimensions]() = 1 calculation in Octopus is not quite the same as performing a [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions) = 3 calculation with a large supercell. In the infinite-supercell limit the two approaches reach the same ground state, but this does not hold for the excited states of the system.\n",
    "\n",
    "Another point worth noting is how the Hartree potential is calculated for periodic systems. In fact the discrete Fourier transform that are used internally in a periodic calculation would always result in a 3D periodic lattice of identical replicas of the simulation box, even if only one or two dimensions are periodic. Fortunately **Octopus** includes a clever system to exactly truncate the long-range part of the Coulomb interaction, in such a way that we can effectively suppress the interactions between replicas of the system along non-periodic axes 1 . This is done automatically, since the value of [PoissonSolver](https://www.octopus-code.org/documentation//13/variables/hamiltonian/poisson/poissonsolver) in a periodic calculation is chosen according to [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions). See also the variable documentation for [PoissonSolver](https://www.octopus-code.org/documentation//13/variables/hamiltonian/poisson/poissonsolver).\n",
    "\n",
    "Atomic positions \n",
    "\n",
    "An important point when dealing with semi-periodic systems is that the coordinates of the atoms along the aperiodic directions must be centered around 0, as this is the case for isolated systems. For slabs, this means that the atoms of the slab must be centered around z=0. If this is not the case, the calculation will lead to wrong results.\n",
    "\n",
    "Sodium chain \n",
    "\n",
    "Let us now calculate some bands for a simple single Na chain (i.e. not a crystal of infinite parallel chains, but just a single infinite chain confined in the other two dimensions)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "from postopus import Run\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.set_option(\"display.max_rows\", 10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir 2-Wires-and-slabes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 2-Wires-and-slabes/"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "## Ground-state"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "First we start we the ground-state calculation using the following input file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = ev_angstrom\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "PeriodicDimensions = 1\n",
    "\n",
    "Spacing = 0.3*angstrom\n",
    "\n",
    "%LatticeParameters\n",
    " 3.9986581*angstrom | 10.58*angstrom | 10.58*angstrom\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    " \"Na\" | 0.0 | 0.0 | 0.0\n",
    "%\n",
    "\n",
    "%KPointsGrid\n",
    " 9 | 1 | 1\n",
    "%\n",
    "KPointsUseSymmetries = yes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "Most of these input variables were already introduced in the [Periodic systems](https://www.octopus-code.org/documentation//13/tutorial/periodic_systems) tutorial. Just note that the ‘‘k’'-points are all along the first dimension, as that is the only periodic dimension.\n",
    "\n",
    "The output should be quite familiar, but there are some noteworthy differences."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_gs.txt | grep -A 4 \"[*] Space [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "Here we see that we are indeed running a 3D system that is periodic along one dimension."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_gs.txt | grep -A 10 \"[*] Lattice [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "Although we specified three lattice parameters in the input file corresponding to three lattice vectors, here the code tells us that it’s only using one lattice vector. This is because the lattice is only periodic along one direction and therefore it is fully determined by the first vector. The other vectors are ignored when generating the Bravais lattice, but they are still used for two other purposes. First, they allow to specify atomic positions in reduced coordinates, which is useful when copying the coordinates from other codes. Second, they specify the lenght of the paralleliped box used to generate the real-space grid. This is confirmed a few lines later:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_gs.txt | grep -A 10 \"[*] Grid [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "Note how the lengths along ‘y’ and ‘z’ are the same as the lenghts of the corresponding lattice parameters specified in the input file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_gs.txt | grep -A 5 \"[*] Hartree [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "Finally, this piece of output confirms that the code is indeed using a cutoff for the calculation of the Hartree potential, as mentioned in the Introduction. The cutoff used is a cylindrical cutoff and, by comparing the FFT grid dimensions with the size of the simulation box, we see that the Poisson solver is using a supercell doubled in size in the y and z directions.\n",
    "\n",
    "At this point you might want to play around with the number of k-points until you are sure the calculation is converged."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "# Band structure"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "We now modify the input file in the following way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_unocc.txt'\n",
    "stderr = 'stderr_unocc.txt'\n",
    "\n",
    "CalculationMode = unocc\n",
    "UnitsOutput = ev_angstrom\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "PeriodicDimensions = 1\n",
    "\n",
    "Spacing = 0.3*angstrom\n",
    "\n",
    "%LatticeParameters\n",
    " 3.9986581*angstrom | 10.58*angstrom | 10.58*angstrom\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    " \"Na\" | 0.0 | 0.0 | 0.0\n",
    "%\n",
    "\n",
    "ExtraStates = 6\n",
    "ExtraStatesToConverge = 4\n",
    "\n",
    "%KPointsPath\n",
    " 14\n",
    " 0.0 | 0.0 | 0.0\n",
    " 0.5 | 0.0 | 0.0\n",
    "%\n",
    "KPointsUseSymmetries = no\n",
    "\n",
    "%Output\n",
    "  dos\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22",
   "metadata": {},
   "source": [
    "and run the code. You might notice the comments on the the LCAO in the output. What’s going on? Why can’t a full initialization with LCAO be done?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23",
   "metadata": {},
   "source": [
    "You can now plot the band structure using the data from the static/bandstructure file, just like in the [Periodic systems](https://www.octopus-code.org/documentation//13/tutorial/periodic_systems) tutorial."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "In the introduction we mentioned that performing a calculation with [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions) = 1 is not quite the same as performing a [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions)= 3 calculation with a large supercell. Lets now check this. Re-run both the ground-state and the unoccupied calculations, but setting [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions) = 3 in the above input files. Before doing so, make sure you copy the static/bandstructure file to a different place so that it is not overwritten (better yet, run the new calculations in a different folder)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26",
   "metadata": {},
   "outputs": [],
   "source": [
    "mkdir 3_PeriodicDimensions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 3_PeriodicDimensions/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs3.txt'\n",
    "stderr = 'stderr_gs3.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = ev_angstrom\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.3*angstrom\n",
    "\n",
    "%LatticeParameters\n",
    " 3.9986581*angstrom | 10.58*angstrom | 10.58*angstrom\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    " \"Na\" | 0.0 | 0.0 | 0.0\n",
    "%\n",
    "\n",
    "%KPointsGrid\n",
    " 9 | 1 | 1\n",
    "%\n",
    "KPointsUseSymmetries = yes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_unocc3.txt'\n",
    "stderr = 'stderr_unocc3.txt'\n",
    "\n",
    "CalculationMode = unocc\n",
    "UnitsOutput = ev_angstrom\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.3*angstrom\n",
    "\n",
    "%LatticeParameters\n",
    " 3.9986581*angstrom | 10.58*angstrom | 10.58*angstrom\n",
    "%\n",
    "\n",
    "%Coordinates\n",
    " \"Na\" | 0.0 | 0.0 | 0.0\n",
    "%\n",
    "\n",
    "ExtraStates = 6\n",
    "ExtraStatesToConverge = 4\n",
    "\n",
    "%KPointsPath\n",
    " 14\n",
    " 0.0 | 0.0 | 0.0\n",
    " 0.5 | 0.0 | 0.0\n",
    "%\n",
    "KPointsUseSymmetries = no\n",
    "\n",
    "%Output\n",
    "  dos\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get Fermi energy as an offset\n",
    "with open(\"static/total-dos-efermi.dat\", \"r\") as file:\n",
    "    EFermi_3d = file.readlines()\n",
    "    parts = EFermi_3d[1].split()\n",
    "    EFermi_3d_value = float(parts[0])\n",
    "with open(\"../static/total-dos-efermi.dat\", \"r\") as file:\n",
    "    EFermi_1d = file.readlines()\n",
    "    parts = EFermi_1d[1].split()\n",
    "    EFermi_1d_value = float(parts[0])\n",
    "print(EFermi_3d_value)\n",
    "print(EFermi_1d_value)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd .."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")\n",
    "run2 = Run(\"3_PeriodicDimensions/\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35",
   "metadata": {},
   "outputs": [],
   "source": [
    "bandstructure_offset = run.default.scf.bandstructure()\n",
    "bandstructure_offset[\n",
    "    [\"band_1\", \"band_2\", \"band_3\", \"band_4\", \"band_5\", \"band_6\"]\n",
    "] += 3.068739  # add fermi energy to all bands\n",
    "\n",
    "bandstructure_offset2 = run2.default.scf.bandstructure()\n",
    "bandstructure_offset2[\n",
    "    [\"band_1\", \"band_2\", \"band_3\", \"band_4\", \"band_5\", \"band_6\"]\n",
    "] += +2.830973  # add fermi energy to all bands"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36",
   "metadata": {},
   "source": [
    "You can see the plot of the two band structures on the right. More comments on this in ref [$^1$](#first_reference)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37",
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = bandstructure_offset.plot(\n",
    "    x=\"kx\",\n",
    "    y=[\"band_1\", \"band_2\", \"band_3\", \"band_4\", \"band_5\"],\n",
    "    xlim=(0, 0.5),\n",
    "    color=\"purple\",\n",
    "    legend=False,\n",
    "    ylabel=\"E (ev)\",\n",
    ")\n",
    "\n",
    "bandstructure_offset2.plot(\n",
    "    x=\"kx\",\n",
    "    y=[\"band_1\", \"band_2\", \"band_3\", \"band_4\", \"band_5\"],\n",
    "    xlim=(0, 0.5),\n",
    "    color=\"green\",\n",
    "    legend=False,\n",
    "    ax=ax,\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38",
   "metadata": {},
   "source": [
    "Band structure for a infinite chain of Sodium atoms, calculated for a single chain (purple lines), and a 3D-periodic crystal of chains in a supercell (green lines).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39",
   "metadata": {},
   "outputs": [],
   "source": [
    "mkdir h-BN_monolayer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd h-BN_monolayer"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41",
   "metadata": {},
   "source": [
    "# h-BN monolayer"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42",
   "metadata": {},
   "source": [
    "Hexagonal boron nitride (h-BN) is an insulator widely studied which has a similar structure to graphene. Here we will describe how to get the band structure of an h-BN monolayer.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43",
   "metadata": {},
   "source": [
    "## Ground-state calculation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44",
   "metadata": {},
   "source": [
    "We will start by calculating the ground-state using the following input file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs_h-BN.txt'\n",
    "stderr = 'stderr_gs_h-BN.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = ev_angstrom\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "PeriodicDimensions = 2\n",
    "\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "BNlength = 1.445*angstrom\n",
    "a = sqrt(3)*BNlength\n",
    "L = 40\n",
    "%LatticeParameters\n",
    " a | a | L\n",
    " %\n",
    "\n",
    "%LatticeVectors\n",
    "  1.0 | 0.0       | 0.0\n",
    " -1/2 | sqrt(3)/2 | 0.0\n",
    "  0.0 | 0.0       | 1.0\n",
    "   %\n",
    "\n",
    "%ReducedCoordinates\n",
    " 'B' | 0.0 | 0.0 | 0.0\n",
    " 'N' | 1/3 | 2/3 | 0.0\n",
    "%\n",
    "\n",
    "PseudopotentialSet = hgh_lda\n",
    "\n",
    "LCAOStart = lcao_states\n",
    "\n",
    "%KPointsGrid\n",
    " 12 | 12 | 1\n",
    "%\n",
    "KPointsUseSymmetries = yes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46",
   "metadata": {},
   "source": [
    "Most of the file should be self-explanatory, but here is a more detailed explanation for some of the choices:\n",
    "\n",
    "[PeriodicDimensions]() = 2: A layer of h-BN is periodic in the ‘‘x’'-‘‘y’’ directions, but not in the ‘‘z’’ direction, so there are two periodic dimensions.\n",
    "\n",
    "[LatticeParameters](): Here we have set the bond length to 1.445 Å. The box size in the ‘‘z’’ direction is ‘‘2 L’’ with ‘‘L’’ large enough to describe a monolayer in the vacuum. Remember that one should always check the convergence of any quantities of interest with the box length value.\n",
    "\n",
    "If you now run the code, you will notice that the cutoff used for the calculation of the Hartree potential is different than for the Sodium chain, as is to be expected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "47",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "48",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stdout_gs_h-BN.txt | grep -A 5 \"[*]Hartree[*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "49",
   "metadata": {},
   "source": [
    "## Band Structure"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50",
   "metadata": {},
   "source": [
    "After the ground-state calculation, we will now calculate the band structure. This is the input for the non-self consistent calculation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp \n",
    "\n",
    "stdout = 'stdout_unocc_h-BN.txt'\n",
    "stderr = 'stderr_unocc_h-BN.txt'\n",
    "\n",
    "CalculationMode = unocc\n",
    "UnitsOutput = ev_angstrom\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "PeriodicDimensions = 2\n",
    "\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "BNlength = 1.445*angstrom\n",
    "a = sqrt(3)*BNlength\n",
    "L=40\n",
    "%LatticeParameters\n",
    " a | a | L\n",
    " %\n",
    "\n",
    "%LatticeVectors\n",
    "  1.0 | 0.0       | 0.0\n",
    " -1/2 | sqrt(3)/2 | 0.0\n",
    "  0.0 | 0.0       | 1.0\n",
    "   %\n",
    "\n",
    "%ReducedCoordinates\n",
    " 'B' | 0.0 | 0.0 | 0.0\n",
    " 'N' | 1/3 | 2/3 | 0.0\n",
    "%\n",
    "\n",
    "PseudopotentialSet = hgh_lda\n",
    "\n",
    "LCAOStart = lcao_states\n",
    "\n",
    "ExtraStatesToConverge = 4\n",
    "ExtraStates = 8\n",
    "\n",
    "%KPointsPath\n",
    " 12  | 7   | 12   # Number of k point to sample each path\n",
    " 0.0 | 0.0 | 0.0  # Reduced coordinate of the 'Gamma' k point\n",
    " 1/3 | 1/3 | 0.0  # Reduced coordinate of the 'K' k point\n",
    " 1/2 | 0.0 | 0.0  # Reduced coordinate of the 'M' k point\n",
    " 0.0 | 0.0 | 0.0  # Reduced coordinate of the 'Gamma' k point\n",
    "%\n",
    "KPointsUseSymmetries = no\n",
    "\n",
    "%Output\n",
    "  dos\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "52",
   "metadata": {},
   "source": [
    "The time required to execute the following run is in the order of two minutes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "54",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs_h-BN_2.txt'\n",
    "stderr = 'stderr_gs_h-BN_2.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = ev_angstrom\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "PeriodicDimensions = 2\n",
    "\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "BNlength = 1.445*angstrom\n",
    "a = sqrt(3)*BNlength\n",
    "L = 40\n",
    "%LatticeParameters\n",
    " a | a | L\n",
    "%\n",
    "\n",
    "%LatticeVectors\n",
    "  1.0 | 0.0       | 0.0\n",
    " -1/2 | sqrt(3)/2 | 0.0\n",
    "  0.0 | 0.0       | 1.0\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " 'B' | 0.0 | 0.0 | 0.0\n",
    " 'N' | 1/3 | 2/3 | 0.0\n",
    "%\n",
    "\n",
    "PseudopotentialSet=hgh_lda\n",
    "\n",
    "LCAOStart=lcao_states\n",
    "\n",
    "ExtraStatesToConverge  = 4\n",
    "ExtraStates  = 8\n",
    "\n",
    "%KPointsPath\n",
    " 12  | 7   | 12   # Number of k point to sample each path\n",
    " 0.0 | 0.0 | 0.0  # Reduced coordinate of the 'Gamma' k point\n",
    " 1/3 | 1/3 | 0.0  # Reduced coordinate of the 'K' k point\n",
    " 1/2 | 0.0 | 0.0  # Reduced coordinate of the 'M' k point\n",
    " 0.0 | 0.0 | 0.0  # Reduced coordinate of the 'Gamma' k point\n",
    "%\n",
    "KPointsUseSymmetries = no\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55",
   "metadata": {},
   "source": [
    "The time requiered to create all runs are in the order of four minutes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "56",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "57",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get Fermi energy as an offset\n",
    "with open(\"static/total-dos-efermi.dat\", \"r\") as file:\n",
    "    EFermi_h_BN = file.readlines()\n",
    "    parts = EFermi_h_BN[1].split()\n",
    "    EFermi_h_BN_value = float(parts[0])\n",
    "EFermi_h_BN_value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")\n",
    "run.default.scf.bandstructure()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "59",
   "metadata": {},
   "outputs": [],
   "source": [
    "bandstructure = run.default.scf.bandstructure() + 5.973251"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60",
   "metadata": {},
   "source": [
    "In this case, we chose the following path to calculate the band structure: Gamma-K, K-M, M-Gamma, with a sampling of 12-7-12 ‘‘k’'-points.\n",
    "\n",
    "Below is the resulting plot of the occupied bands and the first four unoccupied bands from the static/bandstructure file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "61",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "\n",
    "bandstructure.iloc[:, 7:12].plot(color=\"green\", ax=ax, legend=False)\n",
    "bandstructure.iloc[:, 3:7].plot(color=\"purple\", ax=ax, legend=False)\n",
    "plt.ylim(-20, 20)\n",
    "plt.xlim(0, 1)\n",
    "plt.ylabel(\"E (eV)\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62",
   "metadata": {},
   "source": [
    "[Go to *3-Optical-Spectra-Of-Solids.ipynb*](3-Optical-Spectra-Of-Solids.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63",
   "metadata": {},
   "source": [
    "1. C. A. Rozzi, D. Varsano, A. Marini, E. K. U. Gross, and A. Rubio, Exact Coulomb cutoff technique for supercell calculations, [Phys. Rev. B](https://doi.org/10.1103/PhysRevB.73.205119) 73 205119 (2006);\n",
    "<span id=\"first_reference\"></span>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
