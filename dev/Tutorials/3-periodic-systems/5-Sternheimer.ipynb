{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Sternheimer"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "### Preparing the ground state\n",
    "In this tutorial, we are going to calculate the dielectric constant of diamond-structure silicon, using the linear-response Sternheimer equation [$^1$](#first_Reference) (aka density-functional perturbation theory [$^2$](#second_Reference)) and the quantum theory of polarization [$^3$](#third_reference). Like in the tutorial on dynamic polarizabilities of a molecule, we will use an electric field as a perturbation, but unlike in the case of a finite system, we will need to use a different form for the electric field due to the peculiar properties of electric fields in periodic systems. An electric field $\\vec{\\mathcal{E}}$ gives rise to a term in the Hamiltonian $\\vec{\\mathcal{E}} \\cdot \\vec{r}$, but such a perturbation is not periodic. If we use such a perturbation, we lose the ability to use Bloch's Theorem\n",
    "($\\psi_{\\vec{k}}(\\vec{r}) = e^{i\\vec{k}\\vec{r}}*u_{\\vec{k}}(\\vec{r})$)\n",
    "and all the other convenient related formalism for periodic systems. Instead, we can express the Hamiltonian term as\n",
    "$\\vec{\\mathcal{E}} \\cdot i \\vec{\\nabla}_{\\vec{k'}}$, which is now no longer a local potential but instead an operator that applies to the periodic part of the wavefunction $u{\\vec{k}} \\left( \\vec{r} \\right)$. The general theory is laid out in Refs. [$^1$](#first_reference) and [$^4$](#fourth_reference).\n",
    "While some codes such as Quantum ESPRESSO use this formalism to calculate dielectric constants, or Raman intensities (related to \n",
    "$\\frac{\\partial}{\\partial R}$​ for an atomic position **R**), they evaluate the derivative with respect to **k** by finite differences, i.e.\n",
    "$\\frac{\\partial}{\\partial k_i} u_{\\vec{k}} \\approx \\left( u_{\\vec{k} + \\Delta \\vec{k}i} - u_{\\vec{k}} \\right) / \\Delta k_{i} $.\n",
    "\n",
    "In **Octopus**, instead we use linear response to evaluate that derivative, via the same Sternheimer equation we would use for the electric field perturbation. The perturbation in this case, to the effective Bloch Hamiltonian \n",
    "$H_{\\vec{k}} = \\frac{\\hbar^2}{2m}(i \\nabla + \\vec{k} )+ V( \\vec{r}, \\vec{r})$ is\n",
    "$\\frac{\\partial H_{\\vec k}}{\\partial k_i} = -i \\frac{\\partial}{\\partial k_i} + k_i + \\left[ V_{\\rm nl}, r_i \\right]$. The main contribution is from the kinetic energy term, but the potential can contribute as well insofar as it is nonlocal, in which case it is effectively a function of **k** too. The Hartree potential is local. The exchange-correlation potential is local in the Kohn-Sham framework, i.e. LDA, GGA, and meta-GGA functionals, but it is nonlocal when hybrid functionals are used. Moreover, when pseudopotentials are used, as we do in **Octopus**, these are almost always non-local (angular-momentum-dependent) and therefore there is a non-zero commutator to include. This perturbation is referred to as $k \\cdot p$ in **Octopus**, as it is the same one used in the classic $k \\cdot p$ perturbation theory for semiconductor effective masses. Indeed, from our $k \\cdot p$ run, we will be able to obtain group velocities and effective masses -- albeit with some complication in both cases whenever there is a degeneracy, in which case a degenerate perturbation theory needs to be used to consider properly the way that bands split when the k-point changes from the symmetric point. One simplification of using this perturbation compared to other ones is that since this is a non-physical perturbation, i.e. it does not constitute a change of the physical system, but only of an internal parameter we use to describe states, it should not be solved self-consistently, and so the calculation is faster.\n",
    "Once we have obtained $\\frac{u_{n \\vec{k}}}{\\partial k_i}$ for each band *n*, k-point  $\\vec{k}$ , and Cartesian direction $*i*$, we can now calculate the electric field response as we would for a finite system, and obtain a polarizability $\\alpha_{ij}$​. We generally prefer to think in periodic systems of the intensive quantity $\\epsilon_{ij} = 1 + \\frac{4 \\pi}{V} \\alpha_{ij}$​. In the simplest case, we can find the static dielectric constant, i.e. the response at frequency $\\omega = 0$. Note that this includes only the electronic contribution, not any ionic contributions, and thus is referred to as $\\epsilon_\\infty$​ since the frequency is infinite with respect to phonons (but is very small as far as the electrons are concerned). We can also extend this approach straightforwardly to the dielectric function at nonzero frequencies $\\omega$, in which case we are indeed using TDDFT. To prevent divergences at resonant frequencies, and allow us to obtain the imaginary part $\\epsilon_2$​ (related to optical absorption) as well as the real part $\\epsilon_1$​, we introduce a small imaginary part of the frequency $i \\eta$ , which will broaden out these resonances into a Lorentzian lineshape.\n",
    "Now that we have laid out the theory, let's do some calculations. We begin with a ground-state SCF run as usual, with the input file below. We ask for somewhat higher precision than usual, which helps with Sternheimer calculations' convergence later. We use [FilterPotentials](https://www.octopus-code.org/documentation//13/variables/hamiltonian/filterpotentials) = filter_none as this is sometimes helpful for convergence in Sternheimer.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "mkdir 5-Sternheimer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 5-Sternheimer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp \n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "FromScratch = yes\n",
    "\n",
    "CalculationMode = gs\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "a = 10.2\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "%LatticeParameters\n",
    " a | a | a\n",
    " 90 | 90 | 90\n",
    "%\n",
    "Spacing = a/14\n",
    "\n",
    "%ReducedCoordinates\n",
    "  \"Si\" |   0.0       | 0.0       | 0.0\n",
    "  \"Si\" |   1/2       | 1/2       | 0.0\n",
    "  \"Si\" |   1/2       | 0.0       | 1/2\n",
    "  \"Si\" |   0.0       | 1/2       | 1/2\n",
    "  \"Si\" |   1/4       | 1/4       | 1/4\n",
    "  \"Si\" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4\n",
    "  \"Si\" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2\n",
    "  \"Si\" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2\n",
    "%\n",
    "\n",
    "%KPointsGrid\n",
    " 4 | 4 | 4\n",
    " 1/2 | 1/2 | 1/2\n",
    "%\n",
    "\n",
    "KPointsUseSymmetries = yes\n",
    "ExperimentalFeatures = yes\n",
    "SymmetrizeDensity = yes\n",
    "\n",
    "ExtraStates = 1\n",
    "ConvRelDens = 1e-7\n",
    "EigensolverTolerance = 1e-8\n",
    "\n",
    "%Output\n",
    " dos\n",
    "%\n",
    "\n",
    "FilterPotentials = filter_none\n",
    "\n",
    "MixField = density"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "# Performing a k dot calculation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "Performing a k dot p calculation\n",
    "Next, we perform the kdotp run, based on the ground-state results. We need the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode)= kdotp . The [ExtraStates](https://www.octopus-code.org/documentation//13/variables/states/extrastates) is not needed but we will be able to see the velocity and effective mass of the lowest conduction band in this way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_kdot.txt'\n",
    "stderr = 'stderr_kdot.txt'\n",
    "\n",
    "FromScratch = yes\n",
    "\n",
    "CalculationMode = kdotp\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "a = 10.2\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "%LatticeParameters\n",
    " a | a | a\n",
    "%\n",
    "Spacing = a/14\n",
    "\n",
    "%ReducedCoordinates\n",
    "  \"Si\" |   0.0       | 0.0       | 0.0\n",
    "  \"Si\" |   1/2       | 1/2       | 0.0\n",
    "  \"Si\" |   1/2       | 0.0       | 1/2\n",
    "  \"Si\" |   0.0       | 1/2       | 1/2\n",
    "  \"Si\" |   1/4       | 1/4       | 1/4\n",
    "  \"Si\" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4\n",
    "  \"Si\" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2\n",
    "  \"Si\" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2\n",
    "%\n",
    "\n",
    "\n",
    "%KPointsGrid\n",
    " 4 | 4 | 4\n",
    " 1/2 | 1/2 | 1/2\n",
    "%\n",
    "\n",
    "KPointsUseSymmetries = yes\n",
    "SymmetrizeDensity = yes\n",
    "\n",
    "ExtraStates = 1\n",
    "\n",
    "FilterPotentials = filter_none\n",
    "\n",
    "MixField = density"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "Look at the file `dotp/velocity` to find the band velocities for each band and k-point. This is not the typical approach to finding velocities. How else might you obtain them? They play a key role in Boltzmann transport calculations of electric conductivity for example, which you can do in the `BoltzTraP` code.\n",
    "\n",
    "Then, look at the effective masses in the kdotp/kpoint_1_1 etc. This is a $3 \\times 3$ tensor defined from the band energies $\\epsilon_{n \\vec{k}}$​. The inverse effective mass is $m^{-1}{ij} \\left( n, \\vec{k} \\right) = \\frac{1}{\\hbar^2} \\frac{\\partial^2 \\epsilon{n \\vec{k}}}{\\partial k_i \\partial k_j}$. The effective mass tensor itself is obtained by matrix inversion of $m^{-1}$. Note that the complication of degenerate perturbation theory is not dealt with currently in the code (as pointed out in a warning), so focus on either the non-degenerate states, or look at invariants of a degenerate subspace, in which you trace over the degenerate states. The end of the main output file classifies the degenerate subspaces for your convenience."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "# Calculating the linear response\n",
    "\n",
    "Finally, we do a em_resp run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "FromScratch = yes\n",
    "\n",
    "stdout = 'stdout_em_resp.txt'\n",
    "stderr = 'stderr_em_resp.txt'\n",
    "\n",
    "CalculationMode = em_resp\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "a = 10.2\n",
    "\n",
    "BoxShape = parallelepiped\n",
    "%LatticeParameters\n",
    " a | a | a\n",
    "%\n",
    "Spacing = a/14\n",
    "\n",
    "%ReducedCoordinates\n",
    "  \"Si\" |   0.0       | 0.0       | 0.0\n",
    "  \"Si\" |   1/2       | 1/2       | 0.0\n",
    "  \"Si\" |   1/2       | 0.0       | 1/2\n",
    "  \"Si\" |   0.0       | 1/2       | 1/2\n",
    "  \"Si\" |   1/4       | 1/4       | 1/4\n",
    "  \"Si\" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4\n",
    "  \"Si\" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2\n",
    "  \"Si\" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2\n",
    "%\n",
    "\n",
    "\n",
    "%KPointsGrid\n",
    " 4 | 4 | 4\n",
    " 1/2 | 1/2 | 1/2\n",
    "%\n",
    "\n",
    "KPointsUseSymmetries = yes\n",
    "SymmetrizeDensity = yes\n",
    "\n",
    "ExtraStates = 1\n",
    "\n",
    "%EMFreqs\n",
    "2 | 0.0 | 0.1\n",
    "%\n",
    "\n",
    "EMEta = 0.01\n",
    "\n",
    "FilterPotentials = filter_none\n",
    "\n",
    "MixField = density\n",
    "\n",
    "RestartFixedOccupations = no"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "Look in `em_resp/freq_0.000/epsilon` to find the static dielectric constant. You should obtain a diagonal isotropic tensor ($\\epsilon_{xx} = \\epsilon_{yy} = \\epsilon_{zz}$​) as required by the $T_d$​ point group of silicon. The value obtained is somewhat overestimated compared to experiment. Sometimes you will hear people attribute this discrepancy to the \"band-gap\" problem of Kohn-Sham DFT, thinking of a simple perturbation theory expression for the dielectric constant in which a too-small energy denominator will give a too-large result. This reasoning is a misunderstanding however, since the eigenvalues were not used in the calculation of the dielectric constant, and moreover the dielectric constant is a ground-state property which should be calculable according to the Hohenberg-Kohn Theorems regardless of whether electronic excitation energies are computed correctly. We should look instead for the source of any discrepancies in other deficiencies of LDA. We can also look at the variation of ϵ\\epsilonϵ with frequency by checking `em_resp/freq_0.1000/epsilon`, which is the result for frequency $\\hbar \\omega = 0.1 Ha = 2.72 eV$. Is symmetry still preserved? How big are the real and imaginary parts? You will see in the iterations that some lines list a negative number for the state index, i.e. \"-2\": this means the negative frequency for state 2, as we need both $\\omega$ and $−\\omega$ for the dielectric constant calculations. As an exercise, you can calculate values from 0 to 0.2 Ha, with 10 points in between (using the [EMFreqs](https://www.octopus-code.org/documentation//13/variables/linear_response/polarizabilities/emfreqs) block), and make plots of the real and imaginary parts. Compare to results from time-propagation if you have done that tutorial -- the results are identical in principle, but can differ due to the use of iηi \\etaiη here, and the fact that that time-propagation is not linear response and depends somewhat on the strength of the perturbation used.\n",
    "Two final notes: We can study partially periodic systems (e.g. monolayer hexagonal boron nitride) through this same approach, where we use the finite scheme for finite directions, and this $k\\cdot p$ scheme for periodic directions. We can also extend this scheme to the calculation of magnetic susceptibilities, and even to nonlinear properties like magneto-optical susceptibilities [$^5$](#fifth_reference)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "[Back to main tutorial series](../Readme.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "## References"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "1. D. A. Strubbe, L. Lehtovaara, A. Rubio, M. A. L Marques, and S. G. Louie, “Response Functions in TDDFT: Concepts and Implementation,” in Fundamentals of Time-Dependent Density Functional Theory, Lecture Notes in Physics, Springer (2012). [$\\hookleftarrow$](https://www.octopus-code.org/documentation/13/tutorial/periodic_systems/sternheimer/#fnref:1)\n",
    "<span id=\"first_reference\"></span>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "2. S. Baroni, S. de Gironcoli, A. Dal Corso, and P. Gianozzi, “Phonons and related crystal properties from density-functional perturbation theory,” Rev. Mod. Phys. 73, 515 (2001). [$\\hookleftarrow$]()\n",
    "<span id=\"second_reference\"></span>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "3. R. Resta, “Macroscopic polarization in crystalline dielectrics: the geometric phase approach,” Rev. Mod. Phys. 66, 899 (1994). [$\\hookleftarrow$]()\n",
    "<span id=\"third_reference\"></span>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "4. X. Andrade, D. A. Strubbe, U. De Giovannini, A. H. Larsen, M. J. T. Oliveira, J. Alberdi-Rodríguez, A. Varas, I. Theophilou, N. Helbig, M. Verstraete, L. Stella, F. Nogueira, A. Aspuru-Guzik, A. Castro, M. A. L. Marques, and Á. Rubio, “Real-space grids and the Octopus code as tools for the development of new simulation approaches for electronic systems,” Phys. Chem. Chem. Phys. 17, 31371-31396 (2015). [$\\hookleftarrow$]()\n",
    "<span id=\"fourth_reference\"></span>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "5. I. V. Lebedeva, D. A. Strubbe, I. V. Tokatly, and A. Rubio, “Orbital magneto-optical response of periodic insulators from first principles,” npj Comput. Mater. 5, 32 (2019). [$\\hookleftarrow$]()\n",
    "<span id=\"fifth_reference\"></span>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
