{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Detailed overview\n",
    "## Introduction\n",
    "\n",
    "Postopus is a post-processing tool for [Octopus](https://octopus-code.org/) (POSTprocessing for OctoPUS). It provides a user-friendly interface to find and read data written by Octopus throughout a simulation and offers common operations to evaluate this data.\n",
    "\n",
    "Octopus takes an input file describing the systems and the simulation parameters.\n",
    "The input file is called `inp` and is a text file with no extension.\n",
    "The command `octopus` would then have to be executed in the folder that contains this `inp` file.\n",
    "Since octopus doesn't allow custom names for the input file, a possible project structure could look like the following:\n",
    "\n",
    "                  \n",
    "    ├── benzene                    # Project folder\n",
    "    │   ├── benzene.xyz            # Geometry file or other supporting files\n",
    "    │   └── inp                    # Input file\n",
    "    ├── h-atom\n",
    "    │   └── inp\n",
    "    ├── he\n",
    "    │   └── inp\n",
    "    ├── methane                     # Project folder in case of a multi-stage calculation\n",
    "    │   ├── calculation_gs          # Ground state calculation\n",
    "    │   │   └── inp\n",
    "    │   ├── calculation_td          # Time dependent calculation\n",
    "    │   │   └── inp\n",
    "    │   └── inp                     # Input file for the whole calculation (The other files must be placed here one by one in each stage)\n",
    "    └── recipe\n",
    "        └── inp\n",
    "To then run one of these simulations one would run the command `octopus` in the root of the respective folder.\n",
    "\n",
    "## Running the simulation\n",
    "As mentioned before, running a simulation involves two steps:\n",
    "1. Change the directory to the project folder (that contains the `inp` file)\n",
    "2. Run the command `octopus`(optionally store the octopus output in a log file by calling `octopus > out_gs.log 2>&1`). \n",
    "\n",
    "The above two steps could theoretically be executed in a separate shell. However, since the Jupyter notebook has terminal capabilities, it is recommended to execute all steps of a workflow (from defining the input file to the final analysis) in a single notebook (when computationally feasible), as we will do in the following. The notebook then serves as a record of all the steps taken to achieve a particular result. This increases the reproducibility of your work and makes it easier for others to understand and reuse your conclusions. For the latter reason, we also recommend using `!pip freeze` in the notebook to keep track of the software versions of the used Python packages and to print out the Octopus version used to generate the data with `octopus -v`.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus -v"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip freeze"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p \"examples/interference\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd examples/interference/"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "We now create the input file (inp). To do this we use the magic command `%%writefile inp` of the Jupyter Notebook to write the contents of the cell to a file called `inp` (our input file) in the current directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = \"td_stdout.txt\"\n",
    "stderr = \"td_stderr.txt\"\n",
    "\n",
    "CalculationMode = td\n",
    "ExperimentalFeatures = yes\n",
    "FromScratch = yes\n",
    "\n",
    "%Systems\n",
    "  'Maxwell' | maxwell\n",
    "%\n",
    "\n",
    "Maxwell.ParStates = no\n",
    "\n",
    "# Maxwell box variables\n",
    "lsize_mx = 10.0\n",
    "\n",
    "Maxwell.BoxShape = parallelepiped\n",
    "\n",
    "%Maxwell.Lsize\n",
    " lsize_mx | lsize_mx | lsize_mx\n",
    "%\n",
    "\n",
    "dx_mx = 0.5\n",
    "\n",
    "%Maxwell.Spacing\n",
    " dx_mx | dx_mx | dx_mx\n",
    "%\n",
    "\n",
    "# Maxwell calculation variables\n",
    "%MaxwellBoundaryConditions\n",
    " plane_waves | plane_waves | plane_waves\n",
    "%\n",
    "\n",
    "%MaxwellAbsorbingBoundaries\n",
    " not_absorbing | not_absorbing | not_absorbing\n",
    "%\n",
    "\n",
    "# Output variables\n",
    "OutputFormat = plane_x + plane_y + plane_z + axis_x + axis_y + axis_z\n",
    "\n",
    "# Maxwell output variables\n",
    "%MaxwellOutput\n",
    " electric_field\n",
    " magnetic_field\n",
    " maxwell_energy_density\n",
    " trans_electric_field\n",
    "%\n",
    "MaxwellOutputInterval = 10\n",
    "MaxwellTDOutput = maxwell_energy + maxwell_total_e_field + maxwell_total_b_field\n",
    "\n",
    "# Time step variables\n",
    "TDSystemPropagator = prop_expmid\n",
    "dt = 1 / ( sqrt(c^2/dx_mx^2 + c^2/dx_mx^2 + c^2/dx_mx^2) )\n",
    "TDTimeStep = dt\n",
    "TDPropagationTime = 0.35\n",
    "\n",
    "# laser propagates in x direction\n",
    "k_1_x  =  0.707107\n",
    "k_1_y  = -0.707107\n",
    "k_2_x  = -0.447214\n",
    "k_2_y  = -0.223607\n",
    "E_1_z  =  0.5\n",
    "E_2_z  =  0.5\n",
    "pw_1   =  5.0\n",
    "pw_2   =  7.5\n",
    "ps_1_x = -sqrt(1/2) * 20.0\n",
    "ps_1_y =  sqrt(1/2) * 20.0\n",
    "ps_2_x =  sqrt(2/3) * 20.0\n",
    "ps_2_y =  sqrt(1/3) * 20.0\n",
    "\n",
    "%MaxwellIncidentWaves\n",
    "  plane_wave_mx_function | electric_field | 0 | 0 | E_1_z | \"plane_waves_function_1\"\n",
    "  plane_wave_mx_function | electric_field | 0 | 0 | E_2_z | \"plane_waves_function_2\"\n",
    "%\n",
    "\n",
    "%MaxwellFunctions\n",
    "  \"plane_waves_function_1\" | mxf_cosinoidal_wave | k_1_x | k_1_y | 0 | ps_1_x | ps_1_y | 0 | pw_1\n",
    "  \"plane_waves_function_2\" | mxf_cosinoidal_wave | k_2_x | k_2_y | 0 | ps_2_x | ps_2_y | 0 | pw_2\n",
    "%\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "Assuming you have octopus in your PATH:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9",
   "metadata": {},
   "source": [
    "## Loading Data with Postopus\n",
    "To load data with Postopus the path to the output directory of the Octopus simulation is required. In this folder, all output data, as well as the input file `inp` are expected. Data is found automatically and can be discovered by the user by listing all found systems/fields/etc or using auto-completion at run time, e. g. when using Jupyter Notebook.\n",
    "\n",
    "The entry point for users to Postopus is the `Run` class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10",
   "metadata": {},
   "outputs": [],
   "source": [
    "from postopus import Run\n",
    "\n",
    "run = Run()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "The `Run` object discovers available data on the file system and builds a data structure allowing access. If no path to a directory containing the inp file is passed to the run object (i.e. `run = Run()`), then the run object searches in the current working directory. The run object can also be instantiated with a specific path (i.e. `run = Run(\"path/to/inpfile_directory/\")`). In general, the data structure allows choosing data with the following syntax:\n",
    "\n",
    "run.*systemname*.*calculationmode*.*output_name*\n",
    "\n",
    "Parameters set in italics must be replaced with values that mostly correspond to values set in the input file. A closer look at those will be taken in the following sections."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "## System selection\n",
    "\n",
    "The first parameter to select is the system's name. Octopus allows to simulate multiple systems at once, as well as so-called \"multisystem\"s which build a hierarchy of encapsulated systems.  \n",
    "Checking out the \"Systems\" block in the `inp`, the Maxwell system can be found:  \n",
    "```\n",
    "%Systems\n",
    "  'Maxwell' | maxwell\n",
    "%\n",
    "```  \n",
    "One system with the name \"Maxwell\" of type \"maxwell\". The types here are relevant for Octopus, for us the system names are of interest.  \n",
    "Be aware that simulation with Octopus is also possible without setting any systems. In that case, the system's type will be set (by Octopus) to \"electronic_system\". As Postopus requires a name for this system, it will be named \"**default**\" (while not having a name in Octopus). Also, the \"default\" system will always exist, as it is used to store global parameters read from the `inp`, but will never contain any data when the \"Systems\" block is defined in `inp`.\n",
    "\n",
    "Besides reading these names from the `inp`, it also is possible to access this via Postopus. Use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "run"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "## System data - Calculation modes and subsytems\n",
    "\n",
    "To load data from a system, we now call: `run.Maxwell`.\n",
    "This gives a list of the calculation modes which are available via Postopus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.Maxwell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "This is expected, as the `CalculationMode` variable in the `inp` is set to \"td\". As the time-dependent calculation (\"td\") required a previous self-consistent field simulation (\"scf\") this data also could be present in the output folder. If this would be the case, one could see \n",
    "```\n",
    "System(name='Maxwell', rootpath='.'):\n",
    "Found calculation modes:\n",
    "    'scf'\n",
    "    'td'\n",
    "```\n",
    "as output and select between these two. For multisystem examples like the [celestial_bodies tutorial](https://octopus-code.org/documentation/13/tutorial/multisystem/solar_system/), we would also have the keys `Moon`, `Earth` and `Sun` as subsystems, for example `run.SolarSystem.Earth` and `run.SolarSystem.Moon` or, if three levels of nesting are used, `run.SolarSystem.Earth.Terra` and `run.SolarSystem.Earth.Luna`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "## Outputs\n",
    "\n",
    "Getting a list of all available outputs can be done with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.Maxwell.td"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.Maxwell.td.maxwell_energy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "Call the output to get the provided data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.Maxwell.td.maxwell_energy()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22",
   "metadata": {},
   "source": [
    "Octopus produces output files accross multiple folders on the file system. Postopus tries to group them together and provide them in a single object.\n",
    "\n",
    "As example, Octopus outputs the files \"e_field-x\", \"e_field-y\" and \"e_field-z\" (with multiple extensions \".x=0\", \".y=0\", \".x=0,y=0\", ...) for each `n` step of the simulation in \"output_iter/td.0000000\", \"output_iter/td.0000010\", \"output_iter/td.0000020\", ...\n",
    "\n",
    "In Postopus all these files are united and provided by the \"e_field\" output:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.Maxwell.td.e_field"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = run.Maxwell.td.e_field(source=\".x=0\")\n",
    "data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25",
   "metadata": {},
   "source": [
    "The data is provided as xarray.Dataset here. By selecting `data.sel(t=0.21, method=\"nearest\")` one could access the files in \"\"output_iter/td.0000010\". By accessing the component `data.vy` one would read from `e_field-y.x=0`.\n",
    "\n",
    "Note that the data is accessed \"lazily\". This means, as long one does not work with the data no files are accessed. Only when the data is needed (e.g. when doing `data.values`, `data.min()`, ...) loading the files will be invoked.\n",
    "Also only the files which match to the selection are used. In `data.sel(t=0.21, method=\"nearest\").values` files in \"output_iter/td.0000000\" won't be touched.\n",
    "Be aware that accessing the values before selecting specific steps may take a while when dealing with long simulations."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26",
   "metadata": {},
   "source": [
    "The data provided by postopus is in most cases either a [pandas](https://pandas.pydata.org/docs/index.html) DataFrame (as in `run.Maxwell.td.maxwell_energy()`) or an [xarray](https://docs.xarray.dev/en/stable/) DataArray/Dataset (as in `run.Maxwell.td.e_field(source=\".x=0\")`). Outputs returning an xarray object are referred to as \"field\".\n",
    "\n",
    "For outputs where different sources (file extensions) are available the source has to be provided as parameter."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "## Working with field data\n",
    "\n",
    "After we have discovered all available data, we finally want to work with the values.\n",
    "\n",
    "### Get data\n",
    "\n",
    "To get the data we call the output. \n",
    "If we are dealing with `td` data, the call will transform the `step` into time `t` (`step * TDTimestep` (from parser.log)).\n",
    "Our example `inp` has defined `MaxwellOutputInterval` (also could be `OutputInterval`) with a value of 10, meaning Octopus will write all fields every 10 simulation steps."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28",
   "metadata": {},
   "source": [
    "To load the e_field in z direction at the plane xy at z=0 and at t in [0.1, 0.2] we use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29",
   "metadata": {},
   "outputs": [],
   "source": [
    "e_field_plane = (\n",
    "    run.Maxwell.td.e_field(source=\"z=0\").sel(t=[0.1, 0.2], method=\"nearest\").vz\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30",
   "metadata": {},
   "source": [
    "The returned data itself is an object of a `xarray.DataArray`. This object has the following attributes  \n",
    "    - `values` contains the data as a `NumPy` array  \n",
    "    - `coords` provides the correct spatial coordinates for every data point in `values`  \n",
    "    - `dims` gives the number of dimensions for the data, as well as the dimension names  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Xarray object\n",
    "e_field_plane"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Actual values\n",
    "e_field_plane.values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Shape of the values\n",
    "e_field_plane.values.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Dimensions of the data\n",
    "e_field_plane.dims"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Coordinates of the data\n",
    "e_field_plane.coords"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(e_field_plane.coords[\"x\"].shape)\n",
    "print(e_field_plane.coords[\"y\"].shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37",
   "metadata": {},
   "source": [
    "Plotting this could now be done with Matplotlib's `imshow()` or `countour()`, or one could use `xarray`'s plot or `holoviews`. More information in [Plotting Documentation](xarray-plots1.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38",
   "metadata": {},
   "source": [
    "The `source` parameter can be omitted if there is only one source (file-extension), in which case Postopus will use the one available extension for the requested files. If there is more than one source, postopus will throw a `ValueError`, showing the user the different available sources:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39",
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [],
   "source": [
    "run.Maxwell.td.e_field()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40",
   "metadata": {},
   "source": [
    "You can also select the data by index with the `isel` method. The `indices` parameter can be also negative, like in python `list`s. One can also `isel` a `list` of indices or a `slice`. This method could come handy in case you don't want to look up the step number of the last iteration for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.Maxwell.td.e_field(source=\"z=0\").isel(t=-1).vz"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42",
   "metadata": {},
   "source": [
    "The `Field` above is identical as the following one, which holds the data for the last iteration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.Maxwell.td.e_field(source=\"z=0\").sel(t=0.337, method=\"nearest\").vz"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44",
   "metadata": {},
   "source": [
    "Note that selection by step can be enabled by using `set_xindex(\"step\")`. Use with cation (or drop the index at a later point) as now `t` and `step` refers to the same axis which might confuse other python packages (they will try things like selecting `e_field.sel(t=0.5, step=10)`, which then raises an error)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45",
   "metadata": {},
   "outputs": [],
   "source": [
    "run.Maxwell.td.e_field(source=\"z=0\").set_xindex(\"step\").sel(step=160).vz"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46",
   "metadata": {},
   "source": [
    "## Plotting\n",
    "\n",
    "Plotting the data is well integrated with common packages, such as matplotlib and holoviews:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "47",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "e_field_plane = run.Maxwell.td.e_field(source=\"z=0\").sel(t=0.15, method=\"nearest\")\n",
    "\n",
    "plt.imshow(e_field_plane.vz.values);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48",
   "metadata": {},
   "source": [
    "Xarray also provides a convenient `.plot` method. It uses matplotlib internally and makes use of the additional metadata such as dimensions and coordinates to create axis labels and a colorbar. For more details please refer to the [plotting Documentation](xarray-plots1.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "49",
   "metadata": {},
   "outputs": [],
   "source": [
    "e_field_plane.vz.plot(x=\"x\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50",
   "metadata": {},
   "source": [
    "To generate dynamic and/or higher dimensional plots we recommend using `holoviews`, details can be found in the [holoviews tutorial](holoviews_with_postopus.ipynb):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51",
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "\n",
    "hv.extension(\"bokeh\")  # Allow for interactive plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "52",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note for web users: You should have an active notebook session to interact with the plot\n",
    "e_field_over_time = run.Maxwell.td.e_field(source=\"z=0\")\n",
    "hv_ds = hv.Dataset(e_field_over_time.vz)\n",
    "hv_im = hv_ds.to(hv.Image, kdims=[\"x\", \"y\"])\n",
    "hv_im"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "53",
   "metadata": {},
   "source": [
    "## Postprocessing "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54",
   "metadata": {},
   "source": [
    "To manipulate Xarray data is in general fairly simple, as it integrates many built-in methods from the scipy and NumPy libraries among others (see the [documentation](https://docs.xarray.dev/en/stable/user-guide/computation.html)), having the advantage that the operations that involve spatial manipulation are more intuitive (see also for example the [xrft tutorial](xrft.ipynb)). The results of the computations are itself xarrays, so it is still possible to do use all the plotting methods presented above. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "55",
   "metadata": {},
   "outputs": [],
   "source": [
    "integrated_field = e_field_over_time.vz.integrate(coord=\"y\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "56",
   "metadata": {},
   "outputs": [],
   "source": [
    "integrated_field"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "57",
   "metadata": {},
   "source": [
    "As we integrated one of the coordinates, we are dealing now with 1D data that evolves in time, thus we are not going to use holoviews.Images, but holoviews.Curves."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note for web users: You should have an active notebook session to interact with the plot\n",
    "hv_ds_int = hv.Dataset(integrated_field)\n",
    "hv_im_int = hv_ds_int.to(hv.Curve, kdims=[\"x\"], dynamic=True)\n",
    "hv_im_int"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
