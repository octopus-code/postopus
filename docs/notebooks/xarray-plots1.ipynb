{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Plotting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "%config InlineBackend.figure_formats = ['svg']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2",
   "metadata": {},
   "source": [
    "## Plotting with `xarray`\n",
    "### Overall architecture\n",
    "\n",
    "We use the xarray package, which provides some convenience commands to create plots. In this notebook, we use `matplotlib` as the plotting back end.\n",
    "\n",
    "As our plots need to get more sophisticated and tuned, we start using more and more `matplotlib` commands. We show towards the end of the section how to draw the `xarray` data with matplotlib using only `matplotlib` commands for the plots. At the end, we make a reference to the `HoloViews` tutorial, which is especially suited for multidimensional and dynamic plots.\n",
    "\n",
    "### Getting the xarray\n",
    "\n",
    "For now, we load a particular data file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "from postopus import Run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd ../octopus_data/benzene/"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus > out_gs.log 2>&1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "The data set contains the electron density of the Benzene molecule, sampled on a 3d grid. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "To make processing and plotting easier, we ask Postopus for the `xarray` representation of the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa = run.default.scf.density(source=\"cube\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "Calling `density()` provides the data accross all iterations in output_iter/scf.*/density.cube as well as static/density.cube.\n",
    "The iteration step is part of the coordinates of the given DataArray:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa.coords"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa.values.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "Because the xarray object knows the coordinates (and the names of the coordinates, such as `x`, `y` and `z`), it carries the metadata that is required to search and plot the data. This can make the plotting of the data easier. It also helps selecting subsets of the data (which may be plotted subsequently).\n",
    "\n",
    "Here are some examples."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "### Selecting a slice by index\n",
    "\n",
    "First, we only want to work with the data provided in static/ which is always the last iteration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15",
   "metadata": {},
   "outputs": [],
   "source": [
    "xa = xa.isel(step=-1)\n",
    "xa"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "We know we have 33 sampling points in the z direction. We expect the benzene molecule to be in the middle of the sampled region, so we can ask for index $i_z = 33/2 \\approx 16$ to retrieve density data points in the x-y plane where the index for z is 16:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17",
   "metadata": {},
   "outputs": [],
   "source": [
    "s0 = xa.isel(z=16)\n",
    "s0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "`s0` is another `xarray` object. (It provides a *view* on the xarray, so that it does not cost additional memory to create it.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "### Creating a plot quickly (`.plot()`)\n",
    "\n",
    "The quickest way to visualise any xarray object is to call the `plot` method on it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "s0.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "We get a visualisation of the data that has correct coordinate labels in the x and y direction, and the values of the z coordinate at which we sample the data are shown as a title. The colourbar shows the colourscale for the density.\n",
    "\n",
    "There are things one may wish to improve in this plot, for example having an equal aspect ratio (so that a circle in the data would look like a circle when plotted, not like an ellipse). We will see below how this can be achieved. \n",
    "\n",
    "Here, the lesson is: A quick visualisation of the data can be achieved using the `plot` method on an `xarray` object. (Note: the output of the `plot` method will depend on the dimension of the data.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22",
   "metadata": {},
   "source": [
    "### Inverting the order of `x` and `y`\n",
    "\n",
    "As we can see from the image above the `y`-axis is presented on the horizontal axis and the `x`-axis is presented on the vertical axis. This is the [xarray plotting standard](https://docs.xarray.dev/en/stable/generated/xarray.plot.pcolormesh.html), which uses the second dimension for the horizontal axis. In natural sciences, we commonly use the `x` dimension (the first one) as the horizontal dimension and `y` (the second dimension) as the vertical one. If we want to follow this convention, we need to specify the `x`-axis explicitly with `plot(x=\"x\")`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {},
   "outputs": [],
   "source": [
    "s0.plot(x=\"x\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "For simplicity, we will not add the `(x=\"x\")` to every plot of this tutorial, but it can be added where required. We will keep `xarray`'s standard and use `plot()` as it is."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Selecting a slice by coordinate\n",
    "\n",
    "In the example above, we have chosen index 16 in the `xa.isel(z=16)` command to access the plane at $z\\approx 0$.\n",
    "\n",
    "As xarray knows the coordinate values that are attached to our data points, we can also ask it to find the plane for which `z=0`. As the data point that is meant to represent $z=0$ is actually located at $z=1.588\\cdot 10^{-6}$, we need to tell xarray that we will accept the *nearest* data point to $z=0$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26",
   "metadata": {},
   "outputs": [],
   "source": [
    "s1 = xa.sel(z=0, method=\"nearest\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27",
   "metadata": {},
   "outputs": [],
   "source": [
    "s1.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28",
   "metadata": {},
   "source": [
    "### Fine-tuning: no grid lines\n",
    "\n",
    "To get rid of the grid lines in the plot, we can use `plot.imshow()` instead of `plot()` on the xarray object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29",
   "metadata": {},
   "outputs": [],
   "source": [
    "s1.plot.imshow();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30",
   "metadata": {},
   "source": [
    "[The grid lines only seem to appear with the svg plotting backend.]\n",
    "\n",
    "As a second example, let's slice the data in the $x\\approx 0$ plane:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31",
   "metadata": {},
   "outputs": [],
   "source": [
    "s2 = xa.sel(x=0, method=\"nearest\")\n",
    "s2.plot.imshow();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32",
   "metadata": {},
   "source": [
    "### Fine-tuning: equal aspect ratio\n",
    "\n",
    "To draw each pixel as a square, we need to (in matplotlib terminology) set the aspect ratio to `equal`. \n",
    "\n",
    "Once we move from a quick plot of the data to a more refined visualisation, it is worth using the (somewhat standard) matplotlib figure and axes template:\n",
    "\n",
    "1. Using `fig, ax = plt.subplots()` we create a figure (`fig`) and an axes (`ax`) object. The axes object defines the axes which will span our plot. (The name function is called `subplots` because we can create an array of axes objects with it - even though in this example we just want one, and that is why we do not pass any arguments to `subplots`).\n",
    "\n",
    "2. Pass the `ax` object to the xarray drawing routine. \n",
    "\n",
    "3. Modify the `ax` (and if we want the `fig` object) afterwards to fine-tune the plot.\n",
    "\n",
    "Here is our full example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "s1.plot.imshow(ax=ax)\n",
    "ax.set_aspect(\"equal\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34",
   "metadata": {},
   "source": [
    "### Selecting sub-sets of data \n",
    "\n",
    "In our first example, we select data points in the xarray `xa` where the field value is greater than 0.001 (`xa>0.001`), and remove (`drop=True`) all other points.\n",
    "\n",
    "We then slice the remaining data at $z\\approx 0$, and show a plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35",
   "metadata": {},
   "outputs": [],
   "source": [
    "s2 = xa.where(xa > 0.001, drop=True).sel(z=0, method=\"nearest\")\n",
    "s2.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36",
   "metadata": {},
   "source": [
    "We can also use data selection to focus on features of interest. For example, to only display data for which $ -3 \\le x \\le 3$ and $-4 \\le y \\le 4$, and to plot a slice of the data at $z\\approx 0$, we can use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37",
   "metadata": {},
   "outputs": [],
   "source": [
    "s3 = xa.where((abs(xa.x) <= 3) & (abs(xa.y) <= 4), drop=True).sel(z=0, method=\"nearest\")\n",
    "s3.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38",
   "metadata": {},
   "source": [
    "## Plot directly with `matplotlib`\n",
    "\n",
    "Here is an example that re-creates the plots from above using only the data from the xarray object, and plain `matplotlib` plotting commands otherwise.\n",
    "\n",
    "In the example below, the `extend` argument tells the `imshow` command what the x-axis and y-axis values represent."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "\n",
    "# figure out the data (d) and coordinates (x, y):\n",
    "d = s3\n",
    "x = s3.coords[\"x\"].values\n",
    "y = s3.coords[\"y\"].values\n",
    "\n",
    "# create the actual image, with `magma` colour map\n",
    "p = ax.imshow(d, extent=[x.min(), x.max(), y.min(), y.max()], aspect=\"auto\")\n",
    "ax.set_xlabel(\"x\")\n",
    "ax.set_ylabel(\"y\")\n",
    "\n",
    "# add colour bar\n",
    "fig.colorbar(p);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40",
   "metadata": {},
   "source": [
    "As a second example, we draw some contour lines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "\n",
    "# contour needs x and y coordinates on matrices X and Y with the same dimensions as the data\n",
    "Y, X = np.meshgrid(x, y, indexing=\"ij\")\n",
    "\n",
    "p2 = ax.contour(X, Y, d, levels=10)\n",
    "fig.colorbar(p2);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42",
   "metadata": {},
   "source": [
    "We can also combine different representations in the same axis:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(10, 8))\n",
    "\n",
    "z = xa.sel(z=0, method=\"nearest\")\n",
    "x = xa.coords[\"x\"].values\n",
    "y = xa.coords[\"y\"].values\n",
    "p = ax.imshow(\n",
    "    z,\n",
    "    vmin=z.min(),\n",
    "    vmax=z.max(),\n",
    "    extent=[x.min(), x.max(), y.min(), y.max()],\n",
    "    interpolation=\"nearest\",\n",
    "    aspect=\"equal\",\n",
    "    alpha=1,\n",
    ")\n",
    "fig.colorbar(p)\n",
    "\n",
    "Y, X = np.meshgrid(x, y, indexing=\"ij\")\n",
    "p2 = ax.contour(X, Y, z, cmap=\"magma\", levels=5)\n",
    "fig.colorbar(p2);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44",
   "metadata": {},
   "source": [
    "### Changing the colourmap"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45",
   "metadata": {},
   "source": [
    "See [matplotlib documentation for the available colourmaps](https://matplotlib.org/stable/tutorials/colors/colormaps.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "s3.plot.imshow(ax=ax, cmap=\"magma\")\n",
    "ax.set_aspect(\"equal\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47",
   "metadata": {},
   "source": [
    "### Saving a figure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "48",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "s3.plot.imshow(ax=ax, cmap=\"cividis\")\n",
    "ax.set_aspect(\"equal\")\n",
    "\n",
    "fig.savefig(\"test.pdf\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "49",
   "metadata": {},
   "source": [
    "The `fig.savefig` command creates a pdf file with the plot on disk in the current working directory. Other formats can be chosen through the file extension, as is usual for matplotlib's `savefig` command."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50",
   "metadata": {},
   "source": [
    "### Surface plot\n",
    "\n",
    "There are probably better ways to create such plots, but here is an example for completeness."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import cm\n",
    "from matplotlib.ticker import LinearLocator\n",
    "import numpy as np\n",
    "\n",
    "fig, ax = plt.subplots(subplot_kw={\"projection\": \"3d\"}, figsize=(10, 10))\n",
    "\n",
    "# Make data.\n",
    "\n",
    "z = s3\n",
    "x = z.coords[\"x\"].values\n",
    "y = z.coords[\"y\"].values\n",
    "\n",
    "Y, X = np.meshgrid(x, y, indexing=\"ij\")\n",
    "Z = z.values\n",
    "\n",
    "# Plot the surface.\n",
    "surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0, antialiased=False)\n",
    "\n",
    "# Customize the z axis.\n",
    "ax.set_zlim(-1, 1)\n",
    "ax.zaxis.set_major_locator(LinearLocator(11))\n",
    "\n",
    "# Add a color bar which maps values to colors.\n",
    "fig.colorbar(surf, shrink=0.5, aspect=7)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "52",
   "metadata": {},
   "source": [
    "## Plotting with `HoloViews`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "53",
   "metadata": {},
   "source": [
    "For dynamic/multidimensional plots: s. the [holoviews tutorial](holoviews_with_postopus.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
