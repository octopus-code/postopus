Application Examples
--------------------

A short list of application examples of Postopus in research:

- `Volume rendering with the help of Postopus <https://gitlab.com/sohlmann/octopus-volume-rendering>`_
- `PML implementation Analysis with HoloViews and Postopus <https://gitlab.gwdg.de/kevin.yanes-garcia/postopus-2023-masters-project-kevin/-/tree/master/sebastians_use_case/pml_analysis>`_
- `A step-by-step replication of Matplotlib plots with HoloViews and Postopus. <https://gitlab.gwdg.de/kevin.yanes-garcia/postopus-2023-masters-project-kevin/-/tree/master/ilkes-use-case/physical-non-sense-light-data-combined-plotter-tutorial>`_
- `An advanced HoloViews/Postopus example used for replicating a publication figure. <https://gitlab.gwdg.de/kevin.yanes-garcia/postopus-2023-masters-project-kevin/-/tree/master/ilkes-use-case/Time-Resolved-Plasmon-Assisted-Generation-of-Arbitrary-Optical-Vortex-Pulses>`_
