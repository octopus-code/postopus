API Reference
=============

Run
---

.. autoclass:: postopus.octopus_run.Run
    :members:


System and Calculation modes
----------------------------

.. autoclass:: postopus.datacontainers.system.System
    :members:

.. autoclass:: postopus.datacontainers.calculationmodes.CalculationModes
    :members:

Output proxy
------------

.. autoclass:: postopus.datacontainers.output_proxy.OutputProxy
    :members:

.. autoclass:: postopus.datacontainers.output_proxy.field.FieldOutput
    :members:

.. autoclass:: postopus.datacontainers.output_proxy.static.StaticOutput
    :members:

.. autoclass:: postopus.datacontainers.output_proxy.static.Info
    :members:

.. autoclass:: postopus.datacontainers.output_proxy.tdgeneral.TDGeneralVectorField
    :members:
